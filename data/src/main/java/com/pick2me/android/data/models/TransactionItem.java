package com.pick2me.android.data.models;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.Date;

/**
 * Created by abdulmujibaliu on 3/8/17.
 */

public class TransactionItem implements Parcelable {

    private String mObjID, mAmount, mPaymentMethod, mPINCode, mUserID;
    private Date mDate;

    public TransactionItem(String mObjID, String mAmount, String mPaymentMethod, String mPINCode, String mUserID, Date mDate) {
        this.mObjID = mObjID;
        this.mAmount = mAmount;
        this.mPaymentMethod = mPaymentMethod;
        this.mPINCode = mPINCode;
        this.mUserID = mUserID;
        this.mDate = mDate;
    }




    public String getmObjID() {
        return mObjID;
    }

    public void setmObjID(String mObjID) {
        this.mObjID = mObjID;
    }

    public String getmAmount() {
        return mAmount;
    }

    public void setmAmount(String mAmount) {
        this.mAmount = mAmount;
    }

    public String getmPaymentMethod() {
        return mPaymentMethod;
    }

    public void setmPaymentMethod(String mPaymentMethod) {
        this.mPaymentMethod = mPaymentMethod;
    }

    public String getmPINCode() {
        return mPINCode;
    }

    public void setmPINCode(String mPINCode) {
        this.mPINCode = mPINCode;
    }

    public String getmUserID() {
        return mUserID;
    }

    public void setmUserID(String mUserID) {
        this.mUserID = mUserID;
    }

    public Date getmDate() {
        return mDate;
    }

    public void setmDate(Date mDate) {
        this.mDate = mDate;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.mObjID);
        dest.writeString(this.mAmount);
        dest.writeString(this.mPaymentMethod);
        dest.writeString(this.mPINCode);
        dest.writeString(this.mUserID);
        dest.writeLong(this.mDate != null ? this.mDate.getTime() : -1);
    }

    protected TransactionItem(Parcel in) {
        this.mObjID = in.readString();
        this.mAmount = in.readString();
        this.mPaymentMethod = in.readString();
        this.mPINCode = in.readString();
        this.mUserID = in.readString();
        long tmpMDate = in.readLong();
        this.mDate = tmpMDate == -1 ? null : new Date(tmpMDate);
    }

    public static final Creator<TransactionItem> CREATOR = new Creator<TransactionItem>() {
        @Override
        public TransactionItem createFromParcel(Parcel source) {
            return new TransactionItem(source);
        }

        @Override
        public TransactionItem[] newArray(int size) {
            return new TransactionItem[size];
        }
    };
}
