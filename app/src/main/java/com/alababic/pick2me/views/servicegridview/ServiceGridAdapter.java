package com.alababic.pick2me.views.servicegridview;

import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.alababic.pick2me.model.Shop;
import com.bumptech.glide.Glide;
import com.alababic.pick2me.R;
import com.alababic.pick2me.model.Item;
import com.bumptech.glide.request.RequestOptions;

import java.util.List;


public class ServiceGridAdapter extends RecyclerView.Adapter<ServiceGridAdapter.ViewHolder> {

    private List<Item> data;
    private String TAG = getClass().getSimpleName();
    private boolean isGridMode;
    private OnServiceClickListener itemClickListener;
    private boolean isForSetDetails;
    private int gridCount;

    public void setGridCount(int gridCount) {
        this.gridCount = gridCount;
    }

    public boolean isGridMode() {
        return isGridMode;
    }

    public void setGridMode(boolean gridMode) {
        isGridMode = gridMode;
    }

    public ServiceGridAdapter(List<Item> data) {
        this.data = data;
    }


    public void addNewData(List<Item> productItems) {
        data.clear();
        for (Item productItem : productItems) {
            if (!data.contains(productItem)) {
                data.add(productItem);
            }
        }
        reloadData();
    }

    public void addMoreData(List<Item> productItems) {
        for (Item productItem : productItems) {
            if (!data.contains(productItem)) {
                data.add(productItem);
            }
        }
        reloadData();
    }

    public void reloadData() {
        Log.d(TAG, "Loading data of size" + data.size());
        notifyDataSetChanged();
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View v;
        if (isGridMode && (gridCount >= 3)) {
            v = inflater.inflate(R.layout.item_category_small, parent, false);
        } else if (isGridMode && (gridCount < 3)) {
            v = inflater.inflate(R.layout.item_category, parent, false);
        } else {
            v = inflater.inflate(R.layout.item_shop_list, parent, false);
        }

//       else {
//            if (isForSetDetails) {
//                v = inflater.inflate(R.layout.item_card_set_details, parent, false);
//            } else {
//                v = inflater.inflate(R.layout.item_shop_card_favorite, parent, false);
//            }
//        }
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        if (!(data.get(position) == null)) {
            holder.bindData(data.get(position));
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setItemClickListener(OnServiceClickListener itemClickListener) {
        this.itemClickListener = itemClickListener;
    }

    public void removeItemAt(int posiition) {
        data.remove(posiition);
        notifyDataSetChanged();
    }

    /*public void setCountForAllData(int count) {
        for (Item datum : data) {
            datum.setQuantity(count);
        }
        notifyDataSetChanged();
    }*/

    public void setIsForSetDetails(boolean isForSetDetails) {
        this.isForSetDetails = isForSetDetails;
    }

    class ViewHolder extends RecyclerView.ViewHolder {

        private ImageView image;
        private TextView itemName;

        @Nullable
        private TextView shopAddress;
        @Nullable
        private TextView priceStart;
        @Nullable
        private TextView timeAway;
        @Nullable
        private TextView rating;


        public ViewHolder(View itemView) {
            super(itemView);
            this.image = itemView.findViewById(R.id.image);
            this.itemName = itemView.findViewById(R.id.name);
            this.shopAddress = itemView.findViewById(R.id.shop_address);
            this.priceStart = itemView.findViewById(R.id.price_start);
            this.timeAway = itemView.findViewById(R.id.time_away);
            this.rating = itemView.findViewById(R.id.rating);
        }

        public void bindData(final Item data) {
            ViewCompat.setTransitionName(itemView, String.valueOf(getAdapterPosition()) + "_image");
            if (image != null) {
                if (data.getResId() != 0) {
                    Glide.with(this.itemView.getContext())
                            .load(data.getResId())
                            // .bitmapTransform(new RoundedCornersTransformation(holder.itemView.getContext(), 3,0))
                            .into(this.image);
                } else if (data.getImageURL() != null) {
                    RequestOptions options = new RequestOptions();
                    Glide.with(this.itemView.getContext())
                            .load(data.getImageURL())
                            .apply(options.centerCrop())
                            // .bitmapTransform(new RoundedCornersTransformation(holder.itemView.getContext(), 3,0))
                            .into(this.image);
                }
            }

            if (itemName != null) {
                itemName.setText(data.getName());
            }

            this.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (itemClickListener != null) {
                        itemClickListener.onItemClickListener(data, itemView, getAdapterPosition());
                    }
                }
            });

            if (data instanceof Shop) {
                /*if (shopAddress != null) {
                    shopAddress.setText(((Shop) data).getShopAddress());
                }
                if (priceStart != null) {
                    priceStart.setText(((Shop) data).getPriceStart());
                }
                if (timeAway != null) {
                    timeAway.setText(((Shop) data).getTimeAway());
                }
                if (rating != null) {
                    rating.setText(((Shop) data).getRating());
                }*/
            }
        }
    }
}
