package com.alababic.pick2me.internal.di.homefragment;


import com.alababic.pick2me.ui.fragments.home.HomeContract;
import com.alababic.pick2me.ui.fragments.home.HomePresenter;
import com.alababic.pick2me.utils.CategoriesTree;

import dagger.Module;
import dagger.Provides;

@Module
public class HomeModule {

    @Provides
    public CategoriesTree providesRepositoriesTree(){
        return CategoriesTree.getInstance();
    }


    @Provides
    public HomeContract.Presenter providesHomePresenter(HomePresenter presenter){
        return presenter;
    }

}
