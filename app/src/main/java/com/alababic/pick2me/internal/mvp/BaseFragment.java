/*******************************************************************************
 * Copyright (c) 2017 Francisco Gonzalez-Armijo Riádigos
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/

package com.alababic.pick2me.internal.mvp;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringRes;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.internal.mvp.contract.Presentable;
import com.alababic.pick2me.internal.mvp.contract.Viewable;
import com.alababic.pick2me.model.Item;
import com.alababic.pick2me.ui.activities.MainActivity;
import com.alababic.pick2me.views.FragNavTransactionOptions;
import com.hannesdorfmann.fragmentargs.FragmentArgs;

import butterknife.ButterKnife;
import butterknife.Unbinder;

public abstract class BaseFragment<T extends Presentable> extends Fragment implements Viewable<T> {

    private Unbinder unbinder;
    protected T presenter;

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTitle(@StringRes int resource) {
        setTitle(getString(resource));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void setTitle(@NonNull CharSequence msg) {
        getActivity().setTitle(msg);
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStart() {
        super.onStart();
        if(getPresenter()!=null){
            getPresenter().onStart();
        }
    }

    public static final String ARGS_INSTANCE = "com.moehandi.instafragment";

    protected FragmentNavigation fragmentNavigation;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        if (context instanceof FragmentNavigation) {
            fragmentNavigation = (FragmentNavigation) context;
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FragmentArgs.inject(this);
        setRetainInstance(true);
    }

    /**
     * {@inheritDoc}
     */
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(getLayoutId(), container, false);
        unbinder = ButterKnife.bind(this, view);
        //noinspection unchecked
        injectDependencies();

        if(getPresenter()!=null){
            getPresenter().attachView(this);
        }

        return view;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if(getPresenter()!=null){
            getPresenter().onViewCreated();
        }
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroyView() {
        if(getPresenter()!=null){
            getPresenter().detachView();
        }
        if(unbinder!=null){
            unbinder.unbind();
        }
        super.onDestroyView();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onStop() {
        if(getPresenter()!=null){
            getPresenter().onStop();
        }
        super.onStop();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void onDestroy() {
        presenter = null;
        super.onDestroy();
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayError(String message) {
        View rootContent = ButterKnife.findById(getActivity(), android.R.id.content);
        Snackbar.make(rootContent, message, Snackbar.LENGTH_LONG).show();
    }

    public void setTitle(String title) {
        ((MainActivity) getActivity()).updateToolbarTitle(title);
    }

    public interface FragmentNavigation {
        void pushFragment(Fragment fragment);

        void pushFragment(Fragment fragment, FragNavTransactionOptions transactionOptions);

        void popFragment();

        void hideActionBar();

        void showActionBar();

        void makeTransparentStatusBar();

        void reColorStatusBar();

        void showTabBar();

        void hideTabBar();
    }

    public void onItemClickListener(Item item) {

    }

    public void onItemDeleteClickListener(Item item, int position) {

    }

    public void onItemQtyCountChanged(Item item, int position, int quantity) {

    }

    public void onItemCommentBtnClicked(Item item, int position) {

    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void displayError(int messageId) {
        displayError(getString(messageId));
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void showLoading() {
        // no-op by default
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void hideLoading() {
        // no-op by default
    }

    public void injectDependencies(){

    }


    /**
     * {@inheritDoc}
     */
    @Override
    public T getPresenter() {
        return presenter;
    }

    /**
     * {@inheritDoc}
     */
    @Override
    public void injectPresenter(T presenter) {
        this.presenter = presenter;
    }

    protected abstract int getLayoutId();
}
