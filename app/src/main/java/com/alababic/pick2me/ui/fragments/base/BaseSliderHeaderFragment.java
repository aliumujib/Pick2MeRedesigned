package com.alababic.pick2me.ui.fragments.base;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.View;

import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.internal.mvp.contract.Presentable;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;

import java.util.List;

/**
 * Created by aliumujib on 31/03/2018.
 */

public abstract class BaseSliderHeaderFragment extends BaseFragment implements ViewPagerEx.OnPageChangeListener, BaseSliderView.OnSliderClickListener {
    protected AppBarLayout appbar;
    protected CollapsingToolbarLayout collapsingToolbar;
    protected SliderLayout flipperLayout;
    protected ViewPagerIndicator viewPagerIndicator;
    protected Toolbar toolbar;


   /* @Override
    public void onResume() {
        super.onResume();

        fragmentNavigation.hideActionBar();
    }

    @Override
    public void onPause() {
        super.onPause();

        fragmentNavigation.showActionBar();
    }*/


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        this.appbar = view.findViewById(R.id.appbar);
        this.collapsingToolbar = view.findViewById(R.id.collapsing_toolbar);
        this.flipperLayout = view.findViewById(R.id.flipper_layout);
        this.viewPagerIndicator = view.findViewById(R.id.view_pager_indicator);
        this.toolbar = view.findViewById(R.id.toolbar);

        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentNavigation.popFragment();
            }
        });

    }


    public void setSliderPhotos(List<String> sliderPhotos, String waterMarkImage) {
        flipperLayout.setVisibility(View.VISIBLE);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.rectangle_rounded_corners_gray)
                .error(R.drawable.rectangle_rounded_corners_gray);

        viewPagerIndicator.setPageCount(sliderPhotos.size());
        flipperLayout.addOnPageChangeListener(this);

        for (int i = 0; i < sliderPhotos.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            sliderView.setmWaterMarkUrl(waterMarkImage);
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(sliderPhotos.get(i))
                    .setRequestOption(requestOptions)
                    .setBackgroundColor(Color.BLACK)
                    .setProgressBarVisible(false)
                    .setOnSliderClickListener(this);

            //add your extra information
            //sliderView.bundle(new Bundle());
            //sliderView.getBundle().putString("extra", listName.get(i));
            flipperLayout.addSlider(sliderView);
        }
    }


    public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

    }


    public void onPageSelected(int position) {
        viewPagerIndicator.setSelectedIndex(position);
    }


    public void onPageScrollStateChanged(int state) {

    }


    public void onSliderClick(BaseSliderView slider) {

    }
}
