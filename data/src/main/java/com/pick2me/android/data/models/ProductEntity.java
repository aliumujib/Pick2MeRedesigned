package com.pick2me.android.data.models;

/**
 * Created by aliumujib on 11/04/2018.
 */

public class ProductEntity {

    String itemName;

    public String getItemName() {
        return itemName;
    }

    public String mImageUrl;
    public String mSellerName;
    public String mProductCategory;
    public String mProductType;
    public String mProductProducer;
    public String mProductDateOfExpiryYear;
    public String mProductMinimumQty;
    public String mProductPriceString;
    public String mProductDescription;

    public int mProductCategoryType = 0;

    public int getmProductCategoryType() {
        return mProductCategoryType;
    }

    public void setmProductCategoryType(int mProductCategoryType) {
        this.mProductCategoryType = mProductCategoryType;
    }

    public String getmProductDescription() {
        return mProductDescription;
    }

    public void setmProductDescription(String mProductDescription) {
        this.mProductDescription = mProductDescription;
    }

    public String getmProductPriceString() {
        return mProductPriceString;
    }

    public void setmProductPriceString(String mProductPriceString) {
        this.mProductPriceString = mProductPriceString;
    }

    public ProductEntity() {
    }

    public ProductEntity(String mImageUrl, String mItemName, String mSellerName,
                       String mProductCategory, String mProductType, String mProductProducer,
                       String mProductDateOfExpiryYear, String mProductMinimumQty, String mProductPriceString, String mProductDescription) {
        this.mImageUrl = mImageUrl;
        this.itemName = mItemName;
        this.mSellerName = mSellerName;
        this.mProductCategory = mProductCategory;
        this.mProductType = mProductType;
        this.mProductProducer = mProductProducer;
        this.mProductDateOfExpiryYear = mProductDateOfExpiryYear;
        this.mProductMinimumQty = mProductMinimumQty;
        this.mProductPriceString = mProductPriceString;
        this.mProductDescription = mProductDescription;
    }

    public ProductEntity(String mImageUrl, String mItemName, String mSellerName) {
        this.mImageUrl = mImageUrl;
        this.itemName = mItemName;
        this.mSellerName = mSellerName;
    }


    public void setmItemName(String mItemName) {
        this.itemName = mItemName;
    }

    public void setmSellerName(String mSellerName) {
        this.mSellerName = mSellerName;
    }

    public String getmProductProducer() {
        return mProductProducer;
    }

    public void setmProductProducer(String mProductProducer) {
        this.mProductProducer = mProductProducer;
    }

    public String getmProductDateOfExpiryYear() {
        return mProductDateOfExpiryYear;
    }

    public void setmProductDateOfExpiryYear(String mProductDateOfExpiryYear) {
        this.mProductDateOfExpiryYear = mProductDateOfExpiryYear;
    }

    public String getmProductMinimumQty() {
        return mProductMinimumQty;
    }

    public void setmProductMinimumQty(String mProductMinimumQty) {
        this.mProductMinimumQty = mProductMinimumQty;
    }

    public void setItemName(String item_name) {
        this.itemName = item_name;
    }

    public void setmImageUrl(String mImageUrl) {
        this.mImageUrl = mImageUrl;
    }

    public void setSellerName(String seller_name) {
        this.mSellerName = seller_name;
    }

    public void setmProductCategory(String mProductCategory) {
        this.mProductCategory = mProductCategory;
    }

    public void setmProductType(String mProductType) {
        this.mProductType = mProductType;
    }

    public String getmProductType() {
        return mProductType;
    }


    public String getmImageUrl() {
        return mImageUrl;
    }

    public String getmSellerName() {
        return mSellerName;
    }

    public String getmProductCategory() {
        return mProductCategory;
    }

}
