package com.alababic.pick2me.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.TextView;

import com.alababic.pick2me.R;

/**
 * Created by aliumujib on 06/04/2018.
 */

public class ElevatedSearchView extends FrameLayout {
    private View mView;

    public ElevatedSearchView(@NonNull Context context) {
        super(context);
    }

    public ElevatedSearchView(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ElevatedSearchView, 0, 0);

        String search = a.getString(R.styleable.ElevatedSearchView_search_title);
        boolean isEditable = a.getBoolean(R.styleable.ElevatedSearchView_is_editable, false);


        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(R.layout.elevated_search_view, this, true);

        EditText searchText = mView.findViewById(R.id.editText);
        searchText.setText(search);

        if (isEditable) {
            mView.setFocusableInTouchMode(true);
        } else {
            mView.setFocusableInTouchMode(false);
        }

    }

    public ElevatedSearchView(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }


}
