package com.pick2me.android.data.repositories;

import com.pick2me.android.data.contracts.ICartRepository;
import com.pick2me.android.data.models.CartItem;
import com.pick2me.android.data.models.ProductEntity;

import java.util.List;

import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.subjects.ReplaySubject;

/**
 * Created by abdulmujibaliu on 11/13/17.
 */

public class CartRepository implements ICartRepository {

    ReplaySubject<Integer> mCartCountIntegerReplaySubject = ReplaySubject.create();
    private String TAG = getClass().getSimpleName();
    private int cartCountInt = 0;

    public static ICartRepository sharedInstance = new CartRepository();


    public CartRepository() {
        initCartCount();
    }

    @Override
    public void addItemToCart(final ProductEntity productItem) {


    }

    private void incrementCartItemQty(ProductEntity productItem) {

    }

    private Observable<Boolean> getIsItemInCart(final ProductEntity productItem) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final ObservableEmitter<Boolean> observableOnSubscribe) throws Exception {

            }
        });
    }

    private void initCartCount() {

    }

    private void incrementCount() {
        cartCountInt = cartCountInt + 1;
        emitCartCount();
    }

    private void emitCartCount() {
        mCartCountIntegerReplaySubject.onNext(cartCountInt);
    }

    private void decrementCount() {
        cartCountInt = cartCountInt + 1;
        emitCartCount();
    }

    @Override
    public Observable<Integer> getCartCount() {
        return mCartCountIntegerReplaySubject;
    }

    @Override
    public void removeItemFromCart(ProductEntity productItem) {

    }

    @Override
    public Observable<List<CartItem>> getCartItems() {
        return null;
    }
}
