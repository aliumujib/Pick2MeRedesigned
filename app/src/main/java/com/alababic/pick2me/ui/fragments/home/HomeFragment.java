package com.alababic.pick2me.ui.fragments.home;

import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v4.util.Pair;
import android.transition.Fade;
import android.view.View;

import com.alababic.pick2me.AndroidApplication;
import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.di.homefragment.DaggerHomeComponent;
import com.alababic.pick2me.internal.di.homefragment.HomeComponent;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Item;
import com.alababic.pick2me.model.Node;
import com.alababic.pick2me.ui.activities.MainActivity;
import com.alababic.pick2me.ui.fragments.categorydetails.CategoriesDetailFragment;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.views.DetailsTransition;
import com.alababic.pick2me.views.FragNavTransactionOptions;
import com.alababic.pick2me.views.servicegridview.OnServiceClickListener;
import com.nightonke.boommenu.BoomButtons.ButtonPlaceEnum;
import com.nightonke.boommenu.BoomButtons.OnBMClickListener;
import com.nightonke.boommenu.BoomButtons.SimpleCircleButton;
import com.nightonke.boommenu.BoomMenuButton;
import com.nightonke.boommenu.ButtonEnum;
import com.nightonke.boommenu.Piece.PiecePlaceEnum;

import java.util.List;

import javax.inject.Inject;

public class HomeFragment extends BaseFragment<HomeContract.Presenter> implements HomeContract.View {

    int fragCount;
    private BoomMenuButton boomMenuButton;
    private HomeComponent component;

    public static HomeFragment newInstance() {
        Bundle args = new Bundle();
        HomeFragment fragment = new HomeFragment();
        fragment.setArguments(args);
        return fragment;
    }

    public HomeFragment() {
        // Required empty public constructor
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_home;
    }

    @Override
    public void onResume() {
        super.onResume();
        presenter.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
        boomMenuButton.reboom();
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        ((MainActivity) getActivity()).updateToolbarTitle("Pick2MeLogo");

        boomMenuButton = view.findViewById(R.id.bmb);

        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(BaseFragment.ARGS_INSTANCE);
        }

        fragmentNavigation.hideActionBar();


    }

    @Override
    public void initMenuViews(List<Category> categories) {

        //DELAY FOR VIEW INITS
        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {

                boomMenuButton.setButtonEnum(ButtonEnum.SimpleCircle);
                boomMenuButton.setPiecePlaceEnum(PiecePlaceEnum.DOT_7_4);
                boomMenuButton.setButtonPlaceEnum(ButtonPlaceEnum.SC_7_4);
                boomMenuButton.setBackgroundEffect(false);
                boomMenuButton.setShadowEffect(false);
                boomMenuButton.setBoomInWholeScreen(true);
                boomMenuButton.setDimColor(ContextCompat.getColor(getContext(), R.color.transparent));
                boomMenuButton.clearBuilders();

                for (int i = 0; i < categories.size(); i++) {
                    SimpleCircleButton.Builder textInsideCircleButton = new SimpleCircleButton.Builder()
                            .normalImageRes(categories.get(i).getResId())
                            .normalColorRes(R.color.transparent)
                            .shadowEffect(false);

                    if (i == 3) {
                        textInsideCircleButton.buttonRadius(Utils.dpToPx(60));
                    }

                    textInsideCircleButton.listener(new OnBMClickListener() {
                        @Override
                        public void onBoomButtonClick(int index) {
                            presenter.presentDetailFragment(index);
                            //  gotoNextFragment(categories.get(index), index);
                        }
                    });

                    boomMenuButton.addBuilder(textInsideCircleButton);

                    showMenu();
                }

                boomMenuButton.setCancelable(false);
                // boomMenuButton.hideBackGroundView();
            }
        }, 500);

    }

    @Override
    public void gotoNextFragment(Node<Category> category, int index) {
        CategoriesDetailFragment categoriesDetailFragment = CategoriesDetailFragment.newInstance(category, index);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            categoriesDetailFragment.setSharedElementEnterTransition(new DetailsTransition());
            categoriesDetailFragment.setEnterTransition(new Fade());
            setExitTransition(new Fade());
            categoriesDetailFragment.setSharedElementReturnTransition(new DetailsTransition());
        }
        fragmentNavigation.pushFragment(categoriesDetailFragment);
    }

    @Override
    public void showMenu() {

        Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                if (!boomMenuButton.isBoomed()) {
                    presenter.sethasintialized(true);
                    boomMenuButton.boom();
                }
                // boomMenuButton.hideBackGroundView();
            }
        }, 500);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    @Override
    public void setCategoryData(List<Category> categoryData) {
        initMenuViews(categoryData);
    }

    @Override
    public void injectDependencies() {
        super.injectDependencies();

        if (component == null) {
            component = DaggerHomeComponent.builder()
                    .applicationComponent(AndroidApplication.getComponent(getActivity())).build();
            component.inject(this);
        }


    }

    @Inject
    @Override
    public void injectPresenter(HomeContract.Presenter presenter) {
        super.injectPresenter(presenter);
    }


}
