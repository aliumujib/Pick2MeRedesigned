package com.alababic.pick2me.model;

import android.os.Parcel;

/**
 * Created by aliumujib on 31/03/2018.
 */

public class MenuItem implements Item {

    int id;
    String name;
    int resId;
    String imageURL;

    public MenuItem(int id, String name) {
        this.id = id;
        this.name = name;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getResId() {
        return resId;
    }

    @Override
    public String getImageURL() {
        return imageURL;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(this.id);
        dest.writeString(this.name);
        dest.writeInt(this.resId);
        dest.writeString(this.imageURL);
    }

    protected MenuItem(Parcel in) {
        this.id = in.readInt();
        this.name = in.readString();
        this.resId = in.readInt();
        this.imageURL = in.readString();
    }

    public static final Creator<MenuItem> CREATOR = new Creator<MenuItem>() {
        @Override
        public MenuItem createFromParcel(Parcel source) {
            return new MenuItem(source);
        }

        @Override
        public MenuItem[] newArray(int size) {
            return new MenuItem[size];
        }
    };
}
