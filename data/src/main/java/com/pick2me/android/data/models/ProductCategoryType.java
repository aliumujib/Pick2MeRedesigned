package com.pick2me.android.data.models;

/**
 * Created by abdulmujibaliu on 11/13/17.
 */
public enum  ProductCategoryType {

    TYPE_FOOD(1), TYPE_FASHION(2);

    private final int value;

    ProductCategoryType(int value) {
        this.value = value;
    }

    public int getValue() {
        return value;
    }

}
