package com.pick2me.android.data.repositories;

import android.util.Log;


import com.pick2me.android.data.contracts.IBuy2MeRepository;
import com.pick2me.android.data.models.Buy2MeItem;
import com.pick2me.android.data.network.NetworkConstants;
import com.pick2me.android.data.repositories.services.Pick2MeHTTPClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by ABDUL-MUJEEB ALIU on 25/11/2017.
 */

public class Buy2MeRepository implements IBuy2MeRepository {

    private String TAG = getClass().getSimpleName();

    public static IBuy2MeRepository sharedInstance = new Buy2MeRepository();


    @Override
    public Observable<List<Buy2MeItem>> getBuy2MeListData() {
        return Observable.create(new ObservableOnSubscribe<List<Buy2MeItem>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Buy2MeItem>> emitter) throws Exception {
                Pick2MeHTTPClient.get(NetworkConstants.BUY_2_ME_LIST_ENDPOINT, null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            List<Buy2MeItem> buy2MeItemList = new ArrayList<>();
                            for (int i = 0; i < response.length(); i++) {
                                Buy2MeItem buy2MeItem = new Buy2MeItem(response.getJSONObject(i));
                                buy2MeItemList.add(buy2MeItem);
                            }
                            emitter.onNext(buy2MeItemList);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            emitter.onError(e);
                        }
                    }


                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d(TAG, "" + errorResponse);
                        if (errorResponse != null) {
                            emitter.onError(new Throwable(errorResponse.toString()));
                        } else {
                            emitter.onError(new Throwable("Unknown error"));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        emitter.onError(new Throwable(throwable));
                    }
                });
            }
        });
    }

    @Override
    public Observable<Notification<Boolean>> saveBuy2Me() {
        return null;
    }
}
