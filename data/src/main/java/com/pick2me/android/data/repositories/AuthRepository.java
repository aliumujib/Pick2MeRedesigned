package com.pick2me.android.data.repositories;

import android.content.Context;
import android.util.Log;

import com.pick2me.android.data.contracts.IAuthContracts;
import com.pick2me.android.data.models.PasswordResetData;
import com.pick2me.android.data.models.User;
import com.pick2me.android.data.network.NetworkConstants;
import com.pick2me.android.data.repositories.services.Pick2MeHTTPClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONException;
import org.json.JSONObject;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.entity.ContentType;
import cz.msebera.android.httpclient.entity.StringEntity;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by abdulmujibaliu on 11/11/17.
 */

public class AuthRepository implements IAuthContracts.ILoginRepository, IAuthContracts.IForgotPasswordRepository, IAuthContracts.ISignUpRepository {

    public static AuthRepository sharedInstance = new AuthRepository();
    private String TAG = getClass().getSimpleName();
    private Context mContext;


    public AuthRepository() {
        // mContext = context;
    }


    @Override
    public Observable<User> signUserIn(final Context context, final User userdata) {
        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(final ObservableEmitter<User> emitter) throws Exception {
                String dataString = "{\"email\":\"" + userdata.getEmail() + "\",\"password\":\"" + userdata.getPassword() + "\"}";
                HttpEntity dataEntity = new StringEntity(dataString, ContentType.APPLICATION_JSON);
                Log.d(TAG, dataString);

                Pick2MeHTTPClient.post(context, NetworkConstants.USERS_ENDPOINT, dataEntity, ContentType.APPLICATION_JSON.getMimeType(), new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        User user;
                        try {
                            if (response.getBoolean(NetworkConstants.KEY_RESPONSE_TYPE)) {
                                if (response.has(NetworkConstants.KEY_USER)) {
                                    user = new User(response.getJSONObject(NetworkConstants.KEY_USER));
                                    user.setAuthToken(response.getString(NetworkConstants.KEY_AUTH_TOKEN));
                                    emitter.onNext(user);
                                }
                            } else {
                                if (response.has(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)) {
                                    emitter.onError(new Throwable(response.getString(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)));
                                } else {
                                    emitter.onError(new Throwable("Unknown error"));
                                }
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            emitter.onError(e1);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d(TAG, "" + errorResponse);
                        if (errorResponse != null) {
                            emitter.onError(new Throwable(errorResponse.toString()));
                        } else {
                            emitter.onError(new Throwable("Unknown error"));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        emitter.onError(new Throwable(throwable));
                    }
                });
            }
        });
    }

    @Override
    public Observable<User> signUserUp(final Context context, final User user) {
        return Observable.create(new ObservableOnSubscribe<User>() {
            @Override
            public void subscribe(final ObservableEmitter<User> emitter) throws Exception {
                String dataString = "{\"name\":\"" + user.getUsername() + "\", \"password\":\"" + user.getPassword() + "\", \"confirm_password\":\"" + user.getPassword() + "\",\"email\":\"" + user.getEmail() + "\"}";
                HttpEntity dataEntity = new StringEntity(dataString, ContentType.APPLICATION_JSON);
                Log.d(TAG, dataString);

                Pick2MeHTTPClient.post(context, NetworkConstants.USERS_ENDPOINT, dataEntity, ContentType.APPLICATION_JSON.getMimeType(), new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        User user;
                        try {
                            if (response.getBoolean(NetworkConstants.KEY_RESPONSE_TYPE)) {
                                if (response.has(NetworkConstants.KEY_USER)) {
                                    user = new User(response.getJSONObject(NetworkConstants.KEY_USER));
                                    user.setAuthToken(response.getString(NetworkConstants.KEY_AUTH_TOKEN));
                                    emitter.onNext(user);
                                }
                            } else {
                                if (response.has(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)) {
                                    emitter.onError(new Throwable(response.getString(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)));
                                } else {
                                    emitter.onError(new Throwable("Unknown error"));
                                }
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            emitter.onError(e1);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d(TAG, "" + errorResponse);
                        if (errorResponse != null) {
                            emitter.onError(new Throwable(errorResponse.toString()));
                        } else {
                            emitter.onError(new Throwable("Unknown error"));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        emitter.onError(new Throwable(throwable));
                    }
                });
            }
        });
    }

    @Override
    public Observable<Boolean> resetUserPassword(final Context context, final PasswordResetData email) {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final ObservableEmitter<Boolean> emitter) throws Exception {
                String dataString = "{\"email\": \"" + email.getEmail() + "\", \"forgot_password\":\"Y\"}";
                HttpEntity dataEntity = new StringEntity(dataString, ContentType.APPLICATION_JSON);
                Log.d(TAG, dataString);

                Pick2MeHTTPClient.post(context, NetworkConstants.USERS_ENDPOINT, dataEntity, ContentType.APPLICATION_JSON.getMimeType(), new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            if (response.getBoolean(NetworkConstants.KEY_RESPONSE_TYPE)) {
                                emitter.onNext(true);
                            } else {
                                if (response.has(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)) {
                                    emitter.onError(new Throwable(response.getString(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)));
                                } else {
                                    emitter.onError(new Throwable("Unknown error"));
                                }
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            emitter.onError(e1);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d(TAG, "" + errorResponse);
                        if (errorResponse != null) {
                            emitter.onError(new Throwable(errorResponse.toString()));
                        } else {
                            emitter.onError(new Throwable("Unknown error"));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        emitter.onError(new Throwable(throwable));
                    }
                });
            }
        });
    }
}
/*mNetworkHandler.getAuthService().signUserIn(NetworkConstants.getHeaderMap(), user)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/