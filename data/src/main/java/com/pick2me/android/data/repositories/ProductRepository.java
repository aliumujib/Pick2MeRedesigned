package com.pick2me.android.data.repositories;


import com.pick2me.android.data.contracts.IRepoContracts;
import com.pick2me.android.data.models.ProductCategoryType;
import com.pick2me.android.data.models.ProductEntity;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import io.reactivex.Observable;

/**
 * Created by abdulmujibaliu on 10/22/17.
 */

public class ProductRepository implements IRepoContracts.IProductsRepository {


    public static IRepoContracts.IProductsRepository sharedInstance = new ProductRepository();

    @Override
    public Observable<List<ProductEntity>> getProductData() {
        Random rn = new Random();
        int answer = rn.nextInt(1);
        if (answer == 0) {
            return Observable.just(getItems());
        } else {
            return Observable.just(getItems2());
        }
    }

    private List<ProductEntity> getItems2() {
        List<ProductEntity> productItems = new ArrayList<>();
        productItems.add(new ProductEntity("http://www.nourishinteractive.com/system/assets/general/images/foods/plate-of-mixed-dried-bean.gif", "Large beans sack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-protein-meals.png", "Small Peanut Pack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-children-protein-meals.png", "Grainy White beans Platic", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://nutritionmyths.com/wp-content/uploads/2015/08/P0329_beans_legumes_56687253.jpg", "Large Beans Pack", "guegue123", "Beans", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://cf.ltkcdn.net/vegetarian/images/std/125002-425x283-Variety_of_Legumes.jpg", "Variety beans sack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://g6pddeficiency.org/wp/wp-content/uploads/2013/02/legumes.jpg", "Peanut LEgume", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://www.bakeryandsnacks.com/var/plain_site/storage/images/publications/food-beverage-nutrition/bakeryandsnacks.com/markets/snack-makers-target-legumes-for-healthy-npd/7824905-1-eng-GB/Snack-makers-target-legumes-for-healthy-NPD_strict_xxl.jpg", "Large beans sack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Sanck Nut Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));
        return productItems;
    }


    private List<ProductEntity> getItems() {
        List<ProductEntity> productItems = new ArrayList<>();
        ProductEntity productItem = new ProductEntity("https://static-market.jumia.com.ng/p/other-9925-3057221-1-zoom.jpg", "Large Vegetable Pack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact");
        productItem.setmProductCategoryType(ProductCategoryType.TYPE_FASHION.ordinal());
        productItems.add(productItem);

        productItems.add(new ProductEntity("http://www.nourishinteractive.com/system/assets/general/images/foods/legumes-foods-children-protein-meals.png", "Grainy White beans Platic", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://nutritionmyths.com/wp-content/uploads/2015/08/P0329_beans_legumes_56687253.jpg", "Large Beans Pack", "guegue123", "Beans", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://cf.ltkcdn.net/vegetarian/images/std/125002-425x283-Variety_of_Legumes.jpg", "Variety beans sack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://g6pddeficiency.org/wp/wp-content/uploads/2013/02/legumes.jpg", "Peanut LEgume", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("http://www.bakeryandsnacks.com/var/plain_site/storage/images/publications/food-beverage-nutrition/bakeryandsnacks.com/markets/snack-makers-target-legumes-for-healthy-npd/7824905-1-eng-GB/Snack-makers-target-legumes-for-healthy-NPD_strict_xxl.jpg", "Large beans sack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Sanck Nut Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("https://static-market.jumia.com.ng/p/other-9925-3057221-1-zoom.jpg", "Large Vegetable Pack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("https://static-market.jumia.com.ng/p/other-9925-3057221-1-zoom.jpg", "Large Vegetable Pack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        productItems.add(new ProductEntity("https://static-market.jumia.com.ng/p/other-9925-3057221-1-zoom.jpg", "Large Vegetable Pack", "guegue123", "Vegetables", "Other Vegetables", "Gue Gue Nigeria", "2016",
                "2kg", "2000", "Large Vegetable Pack.\n" +
                "Great for people who want to live healthier, Weight loss programs, Elderly etc. Contains a wide variety of local and exotic vegetables which can be combined for lot of recipes. \n" +
                "They include\n" +
                "1 kg Aubergine/Eggplant\n" +
                "1 kg Beetroot\n" +
                "Cabbage (Red)\n" +
                "1 kg Carrot\n" +
                "Cauliflower\n" +
                "Bunch of Celery (local)\n" +
                "1 kg Cucumber\n" +
                "250g Ginger\n" +
                "200g Kale\n" +
                "Lettuce \n" +
                "Bunch Leek/Spring Onions\n" +
                "Mushroom pack\n" +
                "500g Pumpkin/Zucchini\n" +
                "100g Parsley/Mintleaf\n" +
                "500g Radish/Broccoli\n" +
                "200g Spinach\n" +
                "500g Sweet Pepper (Red, Yellow, Orange, Green) \n" +
                "Same-day, Next-day and Planned delivery available\n" +
                "Some of these produce are affected by seasonality... \n" +
                "You can also customize your order, contact"));

        return productItems;
    }


}
