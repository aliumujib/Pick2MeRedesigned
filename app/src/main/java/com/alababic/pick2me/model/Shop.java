package com.alababic.pick2me.model;

import android.os.Parcel;
import android.os.Parcelable;
import android.support.annotation.Nullable;

/**
 * Created by aliumujib on 30/03/2018.
 */

public class Shop implements Parcelable, Item {

    int id;
    String name;
    int resId;
    String imageURL;
    private String shopAddress;
    private String priceStart;
    private String timeAway;
    private String rating;

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setResId(int resId) {
        this.resId = resId;
    }

    public String getShopAddress() {
        return shopAddress;
    }

    public void setShopAddress(String shopAddress) {
        this.shopAddress = shopAddress;
    }

    public String getPriceStart() {
        return priceStart;
    }

    public void setPriceStart(String priceStart) {
        this.priceStart = priceStart;
    }

    public String getTimeAway() {
        return timeAway;
    }

    public void setTimeAway(String timeAway) {
        this.timeAway = timeAway;
    }

    public String getRating() {
        return rating;
    }

    public void setRating(String rating) {
        this.rating = rating;
    }

    public Shop(int id, String name, String imageURL) {
        this.id = id;
        this.name = name;
        this.imageURL = imageURL;
    }

    public Shop(int id, String name, int resID) {
        this.id = id;
        this.name = name;
        this.resId = resID;
    }

    protected Shop(Parcel in) {
        id = in.readInt();
        name = in.readString();
        resId = in.readInt();
        imageURL = in.readString();
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(name);
        parcel.writeInt(resId);
        parcel.writeString(imageURL);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public int getResId() {
        return resId;
    }

    @Override
    public String getImageURL() {
        return imageURL;
    }
}
