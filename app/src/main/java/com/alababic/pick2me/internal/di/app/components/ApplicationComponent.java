package com.alababic.pick2me.internal.di.app.components;

import android.content.Context;

import com.alababic.pick2me.internal.di.app.modules.ApplicationModule;
import com.alababic.pick2me.ui.activities.MainActivity;

import javax.inject.Singleton;

import dagger.Component;

@Component( modules = ApplicationModule.class )
public interface ApplicationComponent {
    void inject(MainActivity activity);
    Context context();
}
