package com.pick2me.android.data.contracts;


import com.pick2me.android.data.models.CategoryEntity;
import com.pick2me.android.data.models.ProductEntity;
import com.pick2me.android.data.models.Shop;

import java.util.List;

import io.reactivex.Observable;

/**
 * Created by abdulmujibaliu on 10/22/17.
 */

public interface IRepoContracts {

    interface ICategoriesRepository {
        Observable<List<CategoryEntity>> getCategoryData();

    }

    interface IProductsRepository {
        Observable<List<ProductEntity>> getProductData();
    }

    interface IShopsRepository {
        Observable<List<Shop>> getShopData();
    }

}
