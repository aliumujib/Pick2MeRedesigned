package com.alababic.pick2me.ui.fragments.categorydetails;

import com.alababic.pick2me.internal.mvp.BasePresenter;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Node;

import java.util.ArrayList;
import java.util.List;

import javax.inject.Inject;

/**
 * Created by aliumujib on 23/04/2018.
 */

public class CategoryPresenter extends BasePresenter<CategoryContract.View> implements CategoryContract.Presenter {
    private Node<Category> category;

    @Inject
    public CategoryPresenter() {

    }

    @Override
    public void onViewCreated() {
        super.onViewCreated();

        List<String> strings = new ArrayList<>();
        strings.add("https://images.pexels.com/photos/927444/pexels-photo-927444.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");
        strings.add("https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");
        strings.add("https://images.pexels.com/photos/582430/pexels-photo-582430.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");
        strings.add("https://images.pexels.com/photos/907142/pexels-photo-907142.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");

        getView().initSliderImages(strings);
    }

    @Override
    public void getCategoryData() {
        getView().setCategoryData(category.getChildrenList());
    }

    @Override
    public void initRootCategoryNode(Node<Category> category) {
        this.category = category;
        getView().setCategoryData(category.getChildrenList());
        getView().initFragmentTitle(category.getData().title);
    }

    @Override
    public void handleCategoryPick(int position) {
        if (category.isLeaf()) {
            getView().gotoNextFragment(category.getData(), position);
        } else {
            if (category.getChildren().get(position).isLeaf()) {
                getView().gotoNextFragment(category.getData(), position);
            } else {
                getView().setCategoryData(category.getChildren().get(position).getChildrenList());
            }
            category = category.getChildren().get(position);
        }
    }

    @Override
    public void handleBackPress() {
        if(category.getParent().isRoot()){
            getView().closeView();
        }else {
            category = category.getParent();
            getView().setCategoryData(category.getChildrenList());
        }
    }


}
