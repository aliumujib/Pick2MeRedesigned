package com.alababic.pick2me.model;
import android.os.Parcel;
import android.os.Parcelable;

public class Category implements Parcelable, Item {

    public int id = -1;
    public String slug = "";
    public String title = "";
    public String description = "";
    public int resID;
    public int parent = -1;
    public int post_count = -1;

    public Category(int id, String title, int resID) {
        this.id = id;
        this.title = title;
        this.resID = resID;
    }

    public Category(int id, String title, String description,int resID) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.resID = resID;
    }

    protected Category(Parcel in) {
        id = in.readInt();
        slug = in.readString();
        title = in.readString();
        description = in.readString();
        parent = in.readInt();
        post_count = in.readInt();
    }

    public static final Creator<Category> CREATOR = new Creator<Category>() {
        @Override
        public Category createFromParcel(Parcel in) {
            return new Category(in);
        }

        @Override
        public Category[] newArray(int size) {
            return new Category[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeString(slug);
        parcel.writeString(title);
        parcel.writeString(description);
        parcel.writeInt(parent);
        parcel.writeInt(post_count);
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public String getName() {
        return title;
    }

    @Override
    public int getResId() {
        return resID;
    }

    @Override
    public String getImageURL() {
        return null;
    }
}
