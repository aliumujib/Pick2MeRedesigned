package com.alababic.pick2me.ui.fragments.authorization.signin;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.ui.fragments.authorization.signup.SignUpFragment;
import com.alababic.pick2me.ui.fragments.home.HomeFragment;
import com.alababic.pick2me.views.ElevatedButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import developer.shivam.crescento.CrescentoContainer;


public class SignInFragment extends BaseFragment {

    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.crescentoContainer)
    CrescentoContainer crescentoContainer;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_user_password)
    EditText inputUserPassword;
    @BindView(R.id.sign_up_btn)
    ElevatedButton signUpBtn;
    @BindView(R.id.sign_up_link)
    TextView signUpLink;
    @BindView(R.id.skip_link)
    TextView skipLink;
    @BindView(R.id.input_and_controls)
    LinearLayout inputAndControls;

    @Override
    public void onResume() {
        super.onResume();

        fragmentNavigation.hideTabBar();
        fragmentNavigation.hideActionBar();
    }

    @Override
    public void onPause() {
        super.onPause();

        fragmentNavigation.showTabBar();
        fragmentNavigation.showActionBar();
    }

    public SignInFragment() {
        // Required empty public constructor
    }

    public static SignInFragment newInstance() {
        SignInFragment fragment = new SignInFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sign_in;
    }

    @OnClick({R.id.back_btn, R.id.sign_up_btn, R.id.sign_up_link, R.id.skip_link})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                fragmentNavigation.popFragment();
                break;
            case R.id.sign_up_btn:
                fragmentNavigation.pushFragment(SignInFragment.newInstance());
                break;
            case R.id.sign_up_link:
                fragmentNavigation.pushFragment(SignUpFragment.newInstance());
                break;
            case R.id.skip_link:
                fragmentNavigation.pushFragment(HomeFragment.newInstance());
                break;
        }
    }
}
