package com.pick2me.android.data.models;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Abdul-Mujeeb Aliu on 7/27/2016.
 */
public class ProfileItem implements Parcelable {
    private String mId, mName, mPicURL, mLstMSG;


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProfileItem that = (ProfileItem) o;

        return mId != null ? mId.equals(that.mId) : that.mId == null;

    }

    @Override
    public int hashCode() {
        return mId != null ? mId.hashCode() : 0;
    }

    protected ProfileItem(Parcel in) {
        mId = in.readString();
        mName = in.readString();
        mPicURL = in.readString();
        mLstMSG = in.readString();
    }

    public static final Creator<ProfileItem> CREATOR = new Creator<ProfileItem>() {
        @Override
        public ProfileItem createFromParcel(Parcel in) {
            return new ProfileItem(in);
        }

        @Override
        public ProfileItem[] newArray(int size) {
            return new ProfileItem[size];
        }
    };

    public String getmId() {
        return mId;
    }

    public ProfileItem() {
    }

    public ProfileItem(String mId, String mName, String mPicURL, String mLstMSG) {
        this.mId = mId;
        this.mName = mName;
        this.mPicURL = mPicURL;
        this.mLstMSG = mLstMSG;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmPicURL() {
        return mPicURL;
    }

    public void setmPicURL(String mPicURL) {
        this.mPicURL = mPicURL;
    }

    public String getmLstMSG() {
        return mLstMSG;
    }

    public void setmLstMSG(String mLstMSG) {
        this.mLstMSG = mLstMSG;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mId);
        dest.writeString(mName);
        dest.writeString(mPicURL);
        dest.writeString(mLstMSG);
    }
}
