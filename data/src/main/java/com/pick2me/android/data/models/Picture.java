package com.pick2me.android.data.models;


import com.pick2me.android.data.network.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abdulmujibaliu on 11/12/17.
 * <p>
 * <p>
 * "picture": {
 * "height": "435",
 * "width": "864",
 * "file_size": "9768",
 * "content_type": "image/png",
 * "subdir": "iblock/461",
 * "file_name": "4619a63b6375537fefbcd412028bc2b1.png",
 * "src": "/upload/iblock/461/4619a63b6375537fefbcd412028bc2b1.png"
 * }
 */

public class Picture {
    String picture, content_type, subdir, file_name, src;
    int height, width, file_size;

    public Picture(JSONObject jsonObject) throws JSONException {
        src = jsonObject.getString(NetworkConstants.KEY_SHOP_PHOTO);
        content_type = jsonObject.getString(NetworkConstants.KEY_SHOP_PHOTO_CONTENT_TYPE);
    }


    public String getPicture() {
        return picture;
    }

    public void setPicture(String picture) {
        this.picture = picture;
    }

    public String getContent_type() {
        return content_type;
    }

    public void setContent_type(String content_type) {
        this.content_type = content_type;
    }

    public String getSubdir() {
        return subdir;
    }

    public void setSubdir(String subdir) {
        this.subdir = subdir;
    }

    public String getFile_name() {
        return file_name;
    }

    public void setFile_name(String file_name) {
        this.file_name = file_name;
    }

    public String getSrc() {
        return src;
    }

    public void setSrc(String src) {
        this.src = src;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getFile_size() {
        return file_size;
    }

    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }
}
