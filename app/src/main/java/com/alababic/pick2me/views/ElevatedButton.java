package com.alababic.pick2me.views;

import android.content.Context;
import android.content.res.TypedArray;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.TextView;
import com.alababic.pick2me.R;

/**
 * Created by aliumujib on 11/04/2018.
 */

public class ElevatedButton extends FrameLayout {

    private TextView btnText;
    private View mView;

    public ElevatedButton(@NonNull Context context) {
        super(context);
    }

    public ElevatedButton(@NonNull Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ElevatedButton, 0, 0);

        String text = a.getString(R.styleable.ElevatedButton_button_title);
        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(R.layout.elevated_btn_layout, this, true);

        btnText = mView.findViewById(R.id.text_view);
        btnText.setText(text);

    }


    @Override
    public void setOnClickListener(@Nullable OnClickListener l) {
        super.setOnClickListener(l);
        btnText.setOnClickListener(l);
    }

    public ElevatedButton(@NonNull Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);

    }


}
