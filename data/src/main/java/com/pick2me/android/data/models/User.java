package com.pick2me.android.data.models;


import com.pick2me.android.data.network.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;


/**
 * Created by abdulmujibaliu on 11/11/17.
 */

public class User {

    String name;
    String password;
    String email;
    String confirm_password;
    String id;
    String authToken;


    public User(JSONObject jsonObject) {
        try {
            name = jsonObject.getString(NetworkConstants.KEY_USERNAME);
            email = jsonObject.getString(NetworkConstants.KEY_USER_EMAIL);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    public User() {
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public User(String name, String password, String email, String confirmpassword) {
        this.name = name;
        this.password = password;
        this.email = email;
        this.confirm_password = confirmpassword;
    }

    public User(String email, String password) {
        this.password = password;
        this.email = email;
    }

    public String getUsername() {
        return name;
    }

    public void setUsername(String username) {
        this.name = username;
    }

    public String getPassword() {
        return password;
    }

    public String getConfirmPassword() {
        return confirm_password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

}
