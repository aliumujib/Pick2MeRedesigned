package com.alababic.pick2me.ui.fragments.categorydetails;

import com.alababic.pick2me.internal.mvp.contract.Presentable;
import com.alababic.pick2me.internal.mvp.contract.Viewable;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Node;

import java.util.List;

/**
 * Created by aliumujib on 23/04/2018.
 */

public interface CategoryContract {

    interface View extends Viewable<Presenter> {

        void initSliderImages(List<String> categoryData);

        void setCategoryData(List<Category> categoryData);

        void gotoNextFragment(Category category, int index);

        void initFragmentTitle(String title);

        void closeView();
    }

    interface Presenter extends Presentable<View> {

        void getCategoryData();

        void initRootCategoryNode(Node<Category> category);

        void handleCategoryPick(int position);

        void handleBackPress();
    }

}
