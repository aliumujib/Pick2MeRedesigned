package com.pick2me.android.data.models;

/**
 * Created by abdulmujibaliu on 3/27/17.
 */
public class CartItem {

    public int buyerDetails;
    public ProductEntity productItem;
    public int qtyToBuy;
    public String productItemName;

    public long id;
    //public boolean isStored;

    public CartItem() {

    }

    public String getProductItemName() {
        return productItem.getItemName();
    }

    public CartItem(long id, ProductEntity productItem, int qtyToBuy) {
        this.productItem = productItem;
        this.productItemName = productItem.getItemName();
        this.qtyToBuy = qtyToBuy;
    }


    public void setProductItemName(String productItemName) {
        this.productItemName = productItemName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof CartItem)) return false;

        CartItem cartItem = (CartItem) o;

        return productItemName.equals(cartItem.productItemName);
    }

    @Override
    public int hashCode() {
        return productItemName.hashCode();
    }

    public int getQtyToBuy() {
        return qtyToBuy;
    }

    public void setQtyToBuy(int qtyToBuy) {
        this.qtyToBuy = qtyToBuy;
    }

    public ProductEntity getProductItem() {
        return productItem;
    }

    public void setProductItem(ProductEntity productItem) {
        this.productItem = productItem;
    }

    public int getBuyerDetails() {
        return buyerDetails;
    }

    public void setBuyerDetails(int buyerDetails) {
        this.buyerDetails = buyerDetails;
    }
}
