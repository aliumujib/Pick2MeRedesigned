package com.alababic.pick2me.model;


import android.os.Parcelable;

/**
 * Created by aliumujib on 30/01/2018.
 */

public interface Item extends Parcelable {

    int getId();

    String getName();

    int getResId();

    String getImageURL();
}
