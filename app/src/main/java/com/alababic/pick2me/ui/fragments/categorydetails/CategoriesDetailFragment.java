package com.alababic.pick2me.ui.fragments.categorydetails;


import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.AndroidApplication;
import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.di.categorydetailfragment.CategoryDetailComponent;
import com.alababic.pick2me.internal.di.categorydetailfragment.DaggerCategoryDetailComponent;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Item;
import com.alababic.pick2me.model.Node;
import com.alababic.pick2me.ui.fragments.shoplist.ShopListFragment;
import com.alababic.pick2me.views.servicegridview.OnServiceClickListener;
import com.alababic.pick2me.views.servicegridview.ServiceGridView;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;
import com.github.vivchar.viewpagerindicator.ViewPagerIndicator;
import com.glide.slider.library.SliderLayout;
import com.glide.slider.library.SliderTypes.BaseSliderView;
import com.glide.slider.library.SliderTypes.TextSliderView;
import com.glide.slider.library.Tricks.ViewPagerEx;

import java.util.List;

import javax.inject.Inject;

import butterknife.BindView;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link CategoriesDetailFragment#newInstance} factory method to
 * create an instance of this fragment.
 */

public class CategoriesDetailFragment extends BaseFragment<CategoryContract.Presenter> implements CategoryContract.View {

    public static String CATEGORY_ITEM = "CATEGORY_ITEM";
    public static String POSITION_KEY = "POSITION_KEY";
    @BindView(R.id.flipper_layout)
    SliderLayout flipperLayout;
    @BindView(R.id.view_pager_indicator)
    ViewPagerIndicator viewPagerIndicator;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.collapsing_toolbar)
    CollapsingToolbarLayout collapsingToolbar;
    @BindView(R.id.appbar)
    AppBarLayout appbar;
    @BindView(R.id.categories_grid)
    ServiceGridView categoriesGrid;
    private CategoryDetailComponent component;
    private CategoryPickerListenerImpl categoriesGridPickerListener;

    public CategoriesDetailFragment() {
        // Required empty public constructor
    }

    public static CategoriesDetailFragment newInstance(Node<Category> item, int position) {
        CategoriesDetailFragment fragment = new CategoriesDetailFragment();
        Bundle args = new Bundle();
        args.putParcelable(CATEGORY_ITEM, item);
        args.putInt(POSITION_KEY, position);
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void injectDependencies() {
        super.injectDependencies();
        if (component == null) {
            component = DaggerCategoryDetailComponent.builder().applicationComponent(AndroidApplication.getComponent(getContext())).build();
            component.inject(this);
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void closeView() {
        fragmentNavigation.popFragment();
    }

    public String capitalize(String str) {
        return str.substring(0, 1).toUpperCase() + str.substring(1);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        int position = -1;
        Bundle bundle = getArguments();
        position = bundle.getInt(POSITION_KEY);
        toolbar.setVisibility(View.GONE);

        if (!presenter.isViewAttached()) {
            presenter.attachView(this);
        }
        presenter.initRootCategoryNode(bundle.getParcelable(CATEGORY_ITEM));
        categoriesGridPickerListener = new CategoryPickerListenerImpl();
        categoriesGrid.setServiceClickListener(categoriesGridPickerListener);


        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {

                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK){
                    // handle back button's click listener
                    presenter.handleBackPress();
                    return true;
                }
                return false;
            }
        });
    }

    @Inject
    @Override
    public void injectPresenter(CategoryContract.Presenter presenter) {
        super.injectPresenter(presenter);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_restaurants;
    }

    @Override
    public void onResume() {
        super.onResume();
        fragmentNavigation.showActionBar();
    }

    @Override
    public void initSliderImages(List<String> stringList) {
        setSliderPhotos(stringList, "");
    }

    @Override
    public void setCategoryData(List<Category> categoryData) {
        categoriesGrid.addData(categoryData);
    }

    public void setSliderPhotos(List<String> sliderPhotos, String waterMarkImage) {
        flipperLayout.setVisibility(View.VISIBLE);
        RequestOptions requestOptions = new RequestOptions();
        requestOptions = requestOptions
                .centerCrop()
                .diskCacheStrategy(DiskCacheStrategy.NONE)
                .placeholder(R.drawable.rectangle_rounded_corners_gray)
                .error(R.drawable.rectangle_rounded_corners_gray);

        viewPagerIndicator.setPageCount(sliderPhotos.size());
        flipperLayout.addOnPageChangeListener(new OnPageChangeListenerImpl());

        for (int i = 0; i < sliderPhotos.size(); i++) {
            TextSliderView sliderView = new TextSliderView(getContext());
            sliderView.setmWaterMarkUrl(waterMarkImage);
            // if you want show image only / without description text use DefaultSliderView instead

            // initialize SliderLayout
            sliderView
                    .image(sliderPhotos.get(i))
                    .setRequestOption(requestOptions)
                    .setBackgroundColor(Color.BLACK)
                    .setProgressBarVisible(false)
                    .setOnSliderClickListener(new OnSliderClickListenerImpl());

            //add your extra information
            //sliderView.bundle(new Bundle());
            //sliderView.getBundle().putString("extra", listName.get(i));
            flipperLayout.addSlider(sliderView);
        }
    }

    @Override
    public void gotoNextFragment(Category category, int index) {

    }




    @Override
    public void initFragmentTitle(String title) {
        setTitle(capitalize(title));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // TODO: inflate a fragment view
        View rootView = super.onCreateView(inflater, container, savedInstanceState);
        return rootView;
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }


    class OnPageChangeListenerImpl implements ViewPagerEx.OnPageChangeListener {

        @Override
        public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

        }

        @Override
        public void onPageSelected(int position) {

        }

        @Override
        public void onPageScrollStateChanged(int state) {

        }
    }


    class OnSliderClickListenerImpl implements BaseSliderView.OnSliderClickListener {
        @Override
        public void onSliderClick(BaseSliderView slider) {

        }
    }

    class CategoryPickerListenerImpl implements OnServiceClickListener{

        @Override
        public void onItemClickListener(Item item, View sharedView, int position) {
            presenter.handleCategoryPick(position);
        }

        @Override
        public void onItemDeleteClickListener(Item item, int position) {

        }

        @Override
        public void onItemQtyCountChanged(Item item, int position, int quantity) {

        }

        @Override
        public void onItemCommentBtnClicked(Item item, int position) {

        }
    }

}
