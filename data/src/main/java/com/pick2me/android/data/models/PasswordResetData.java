package com.pick2me.android.data.models;

/**
 * Created by abdulmujibaliu on 11/11/17.
 */

public class PasswordResetData {
    String email;
    String forgot_password;

    public PasswordResetData(String email, String forgot_password) {
        this.email = email;
        this.forgot_password = forgot_password;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
