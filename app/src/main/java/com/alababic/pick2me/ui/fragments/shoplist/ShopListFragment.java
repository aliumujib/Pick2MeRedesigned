package com.alababic.pick2me.ui.fragments.shoplist;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.alababic.pick2me.R;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Shop;
import com.alababic.pick2me.views.servicegridview.ServiceGridView;

import java.util.ArrayList;
import java.util.List;

public class ShopListFragment extends Fragment {

    private ServiceGridView shopList;

    public ShopListFragment() {
    }

    public static ShopListFragment newInstance(Category category, int index) {
        ShopListFragment fragment = new ShopListFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (getArguments() != null) {
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_shop_list, container, false);
        this.shopList = view.findViewById(R.id.shop_list);
        // Set the adapter


        List<Shop> shops = new ArrayList<>();
        shops.add(new Shop(0, "Burger King", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));
        shops.add(new Shop(1, "Mac Donalds", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));
        shops.add(new Shop(2, "The Burger", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));
        shops.add(new Shop(3, "Rafiiiiiii Shop", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));
        shops.add(new Shop(4, "Shoprite Restaurant 5", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));
        shops.add(new Shop(5, "Shop 6", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));
        shops.add(new Shop(6, "Shop 7", "https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350"));

        shopList.addData(shops);


        return view;
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);

    }

    @Override
    public void onDetach() {
        super.onDetach();
    }

}
