package com.alababic.pick2me.views.servicegridview;

import android.app.Activity;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.res.TypedArray;
import android.support.annotation.DrawableRes;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;


import com.alababic.pick2me.R;
import com.alababic.pick2me.model.Item;
import com.alababic.pick2me.utils.SpacesItemDecoration;
import com.alababic.pick2me.utils.Utils;

import java.util.ArrayList;
import java.util.List;

import xyz.cybersapien.recyclerele.RecyclerELEAdapter;

/**
 * Created by aliumujib on 25/01/2018.
 */

public class ServiceGridView extends LinearLayout {

    private boolean isForSetDetails;
    private boolean isGridMode;
    private boolean showSectionTitle;
    private OnServiceClickListener onServiceClickListener;
    private int gridCount = 2;


    public RecyclerELEAdapter wrapInELEAdapter(RecyclerView.Adapter adapter, RecyclerView recyclerView) {
        View loadingViewDrugs = getActivity().getLayoutInflater().inflate(R.layout.loading_layout, recyclerView, false);
        View emptyViewDrugs = getActivity().getLayoutInflater().inflate(R.layout.empty_error_layout, recyclerView, false);
        View errorViewDrugs = getActivity().getLayoutInflater().inflate(R.layout.empty_error_layout, recyclerView, false);
        return new RecyclerELEAdapter(adapter, emptyViewDrugs, loadingViewDrugs, errorViewDrugs);
    }

    public Activity getActivity() {
        Context context = getContext();
        while (context instanceof ContextWrapper) {
            if (context instanceof Activity) {
                return (Activity) context;
            }
            context = ((ContextWrapper) context).getBaseContext();
        }
        return null;
    }


    public ServiceGridView(Context context) {
        super(context);
    }


    private String TAG = getClass().getSimpleName();
    private View mView;
    private TextView mSectionTitleView, mSectionDescription;
    private RecyclerView mItemsRV;
    public static int INVALID_REQUEST_CODE_NUMBER = -2;

    protected ServiceGridAdapter serviceGridAdapter;
    protected RecyclerView.LayoutManager mLayoutManager;
    private RecyclerELEAdapter mRecyclerELEAdapter;
    private String sectionDescription;
    private View mSectionTitleContainer;
    private String sectionTitle;
    protected List<? extends Item> mDataSource = new ArrayList<>();

    public RecyclerView getmItemsRV() {
        return mItemsRV;
    }

    public void removeitemAtPosition(int posiition) {
        serviceGridAdapter.removeItemAt(posiition);
    }

    public void setGridMode(boolean gridMode) {
        isGridMode = gridMode;
        if (gridMode) {
            initRVGrid();
        } else {
            initRVList();
        }
    }

    public ServiceGridView(Context context, AttributeSet attrs) {
        super(context, attrs);

        TypedArray a = context.obtainStyledAttributes(attrs,
                R.styleable.ServiceGridView, 0, 0);


        sectionTitle = a.getString(R.styleable.ServiceGridView_grid_title);
        showSectionTitle = a.getBoolean(R.styleable.ServiceGridView_show_grid_title, false);
        isGridMode = a.getBoolean(R.styleable.ServiceGridView_is_grid_mode, true);
        isForSetDetails = a.getBoolean(R.styleable.ServiceGridView_is_for_set_details, false);
        gridCount = a.getInt(R.styleable.ServiceGridView_grid_count, 2);

        a.recycle();

        LayoutInflater inflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);

        mView = inflater.inflate(R.layout.titled_recycler_view, this, true);

        mSectionTitleView = (TextView) mView.findViewById(R.id.section_title);
        mSectionTitleContainer = (View) mView.findViewById(R.id.section_title_cont);
        mSectionDescription = (TextView) mView.findViewById(R.id.section_description);
        mItemsRV = (RecyclerView) mView.findViewById(R.id.section_items_recycler);


        if (isGridMode) {
            initRVGrid();
        } else {
            initRVList();
        }

        if (!showSectionTitle) {
            mSectionTitleView.setVisibility(GONE);
            mSectionTitleContainer.setVisibility(GONE);
        }

        if (sectionTitle != null && !sectionTitle.matches(""))
            mSectionTitleView.setText(sectionTitle);

        // mRecyclerELEAdapter = wrapInELEAdapter(serviceGridAdapter, )

        if (sectionDescription != null && !sectionDescription.matches(""))
            mSectionDescription.setText(sectionDescription);


    }


    public void setBackGroundDrawble(@DrawableRes int backGroundDrawble) {
        mView.setBackgroundResource(backGroundDrawble);
    }

    public String getSectionDescription() {
        return sectionDescription;
    }

    public void setSectionDescription(String sectionDescription) {
        this.sectionDescription = sectionDescription;
        mSectionDescription.setText(sectionDescription);
    }

    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
        mSectionTitleView.setText(sectionTitle);
    }

    public List<? extends Item> getmDataSource() {
        return mDataSource;
    }

    public void addData(List<? extends Item> productItems) {
        this.mDataSource = productItems;
        this.serviceGridAdapter.addNewData((List<Item>) productItems);
        serviceGridAdapter.reloadData();
    }

    public void addMoreData(List<Item> productItems) {
        this.serviceGridAdapter.addMoreData(productItems);
        serviceGridAdapter.reloadData();
    }

    public void setServiceClickListener(OnServiceClickListener onSearchResultClickListener) {
        this.onServiceClickListener = onSearchResultClickListener;
        this.serviceGridAdapter.setItemClickListener(onSearchResultClickListener);
    }

    protected void initRVGrid() {
        mLayoutManager = new GridLayoutManager(getContext(), gridCount);
        mItemsRV.setLayoutManager(mLayoutManager);
        mItemsRV.setHasFixedSize(true);
        mItemsRV.setNestedScrollingEnabled(false);
        serviceGridAdapter = new ServiceGridAdapter((List<Item>) mDataSource);
        serviceGridAdapter.setGridMode(isGridMode);
        serviceGridAdapter.setGridCount(gridCount);
        mItemsRV.setAdapter(serviceGridAdapter);
        serviceGridAdapter.reloadData();
        mItemsRV.addItemDecoration(new SpacesItemDecoration(Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4), Utils.dpToPx(4), true));
    }

    protected void initRVList() {
        mLayoutManager = new LinearLayoutManager(getContext());
        mItemsRV.setLayoutManager(mLayoutManager);
        mItemsRV.setHasFixedSize(true);
        mItemsRV.setNestedScrollingEnabled(false);
        serviceGridAdapter = new ServiceGridAdapter((List<Item>) mDataSource);
        serviceGridAdapter.setGridMode(isGridMode);
        serviceGridAdapter.setIsForSetDetails(isForSetDetails);
        mItemsRV.setAdapter(serviceGridAdapter);
        serviceGridAdapter.reloadData();
        mItemsRV.addItemDecoration(new SpacesItemDecoration(Utils.dpToPx(8), Utils.dpToPx(4), Utils.dpToPx(8), Utils.dpToPx(8), false));
    }

    public void setCountForAllData(int countForAllData) {
      //  serviceGridAdapter.setCountForAllData(countForAllData);
    }

}
