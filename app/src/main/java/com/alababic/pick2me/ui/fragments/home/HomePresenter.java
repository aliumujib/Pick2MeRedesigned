package com.alababic.pick2me.ui.fragments.home;

import android.util.Log;

import com.alababic.pick2me.internal.mvp.BasePresenter;
import com.alababic.pick2me.utils.CategoriesTree;

import javax.inject.Inject;

/**
 * Created by aliumujib on 23/04/2018.
 */

public class HomePresenter extends BasePresenter<HomeContract.View> implements HomeContract.Presenter {

    CategoriesTree categoriesTree;
    private boolean hasIntialized;

    @Inject
    public HomePresenter(CategoriesTree categoriesTree) {
        this.categoriesTree = categoriesTree;
    }

    @Override
    public void getCategoryData() {
        Log.d(TAG, String.valueOf(categoriesTree.getRootNode().getChildrenList().size()));
        getView().setCategoryData(categoriesTree.getRootNode().getChildrenList());
    }

    @Override
    public void sethasintialized(boolean hasInit) {
        this.hasIntialized = hasInit;
    }

    @Override
    public void presentDetailFragment(int index) {
        getView().gotoNextFragment(categoriesTree.getRootNode().getChildAtIndex(index), index);
    }



    @Override
    public void onResume() {
        super.onResume();
        getCategoryData();
    }
}
