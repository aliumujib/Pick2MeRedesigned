package com.alababic.pick2me.utils;

import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by aliumujib on 20/03/2018.
 */

public class SpacesItemDecoration extends RecyclerView.ItemDecoration {
    private final int spaceTop;
    private final int spaceBottom;
    private final int spaceLeft;
    private final int spaceRight;
    private boolean isGrid;

    public SpacesItemDecoration(int space, boolean isGrid) {
        this.spaceTop = space;
        this.spaceBottom = space;
        this.spaceLeft = space;
        this.spaceRight = space;
        this.isGrid = isGrid;
    }

    public SpacesItemDecoration(int spaceTop, int spaceBottom, int spaceLeft, int spaceRight, boolean isGrid) {
        this.spaceTop = spaceTop;
        this.spaceBottom = spaceBottom;
        this.spaceLeft = spaceLeft;
        this.spaceRight = spaceRight;
        this.isGrid = isGrid;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {
        outRect.left = spaceLeft;
        outRect.right = spaceRight;
        outRect.bottom = spaceBottom;

       if(!isGrid){
           // Add top margin only for the first item to avoid double space between items
           if (parent.getChildAdapterPosition(view) == 0) {
               outRect.top = spaceTop;
           }
       }else {
           outRect.top = spaceTop;
       }
    }
}