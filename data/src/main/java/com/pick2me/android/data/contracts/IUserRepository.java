package com.pick2me.android.data.contracts;


import com.pick2me.android.data.models.User;

import io.reactivex.Notification;
import io.reactivex.Observable;

/**
 * Created by ABDUL-MUJEEB ALIU on 27/11/2017.
 */

public interface IUserRepository {

    Observable<Notification<User>> getCurrentUser();

    Observable<Notification<Boolean>> saveCurrentUser(User user);

    Observable<Boolean> isUserSignedIn();

    Observable<Notification<Boolean>> updateCurrentUserAuthToken(String authToken);
}
