package com.alababic.pick2me.ui.fragments.authorization.signup;


import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.ui.fragments.authorization.signin.SignInFragment;
import com.alababic.pick2me.ui.fragments.home.HomeFragment;
import com.alababic.pick2me.views.ElevatedButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import developer.shivam.crescento.CrescentoContainer;


public class SignUpFragment extends BaseFragment {
    @BindView(R.id.back_btn)
    ImageView backBtn;
    @BindView(R.id.crescentoContainer)
    CrescentoContainer crescentoContainer;
    @BindView(R.id.input_email)
    EditText inputEmail;
    @BindView(R.id.input_user_password)
    EditText inputUserPassword;
    @BindView(R.id.input_user_repeat_password)
    EditText inputUserRepeatPassword;
    @BindView(R.id.sign_up_btn)
    ElevatedButton signUpBtn;
    @BindView(R.id.input_and_controls)
    LinearLayout inputAndControls;
    @BindView(R.id.sign_in_link)
    TextView signInLink;
    @BindView(R.id.skip_link)
    TextView skipLink;

    public SignUpFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();

        fragmentNavigation.hideTabBar();
        fragmentNavigation.hideActionBar();
    }

    @Override
    public void onPause() {
        super.onPause();

        fragmentNavigation.showTabBar();
        fragmentNavigation.showActionBar();
    }

    public static SignUpFragment newInstance() {
        SignUpFragment fragment = new SignUpFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }


    @Override
    protected int getLayoutId() {
        return R.layout.fragment_sign_up;
    }


    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @OnClick({R.id.back_btn, R.id.sign_up_btn, R.id.sign_in_link, R.id.skip_link})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.back_btn:
                fragmentNavigation.popFragment();
                break;
            case R.id.sign_up_btn:
                fragmentNavigation.pushFragment(SignUpFragment.newInstance());
                break;
            case R.id.sign_in_link:
                fragmentNavigation.pushFragment(SignInFragment.newInstance());
                break;
            case R.id.skip_link:
                fragmentNavigation.pushFragment(HomeFragment.newInstance());
                break;
        }
    }
}
