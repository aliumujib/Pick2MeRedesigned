package com.alababic.pick2me.internal.di.categorydetailfragment;


import com.alababic.pick2me.ui.fragments.categorydetails.CategoryContract;
import com.alababic.pick2me.ui.fragments.categorydetails.CategoryPresenter;
import com.alababic.pick2me.ui.fragments.home.HomeContract;
import com.alababic.pick2me.ui.fragments.home.HomePresenter;
import com.alababic.pick2me.utils.CategoriesTree;

import dagger.Module;
import dagger.Provides;

@Module
public class CategoryModule {


    @Provides
    public CategoryContract.Presenter providesCategoryPresenter(CategoryPresenter presenter){
        return presenter;
    }

}
