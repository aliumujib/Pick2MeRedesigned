package com.alababic.pick2me.internal.di.app.modules;

import android.content.Context;

import com.alababic.pick2me.AndroidApplication;
import com.pick2me.android.data.contracts.IUserRepository;
import com.pick2me.android.data.repositories.AuthRepository;
import com.pick2me.android.data.repositories.UserRepository;
import com.pick2me.android.data.service.adapter.RestFactory;
import com.pick2me.android.data.service.adapter.RetrofitFactory;

import javax.inject.Named;
import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

@Module
public class ApplicationModule {

    private final AndroidApplication application;

    public ApplicationModule(AndroidApplication application) {
        this.application = application;
    }

    @Provides
    Context provideApplicationContext() {
        return this.application;
    }

    @Provides
     @Named("AvailableProcessors")
    int provideAvailableProcessors() {
        return Runtime.getRuntime().availableProcessors() + 1;
    }

    @Provides
    RestFactory provideRestFactory(RetrofitFactory factory) {
        return factory;
    }

    @Provides
     @Named("ApiUrl")
    String provideApiUrl() {
        //return BuildConfig.API_URL;

        return null;
    }

    @Provides
    AuthRepository providePostRepository(AuthRepository postRepository) {
        return postRepository;
    }


    @Provides
    IUserRepository provideUserRepository(UserRepository userRepository) {
        return userRepository;
    }

}
