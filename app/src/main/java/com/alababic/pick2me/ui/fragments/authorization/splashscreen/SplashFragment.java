package com.alababic.pick2me.ui.fragments.authorization.splashscreen;


import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.ui.fragments.authorization.signin.SignInFragment;
import com.alababic.pick2me.ui.fragments.authorization.signup.SignUpFragment;
import com.alababic.pick2me.views.ElevatedButton;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import developer.shivam.crescento.CrescentoContainer;

public class SplashFragment extends BaseFragment {

    @BindView(R.id.crescentoContainer)
    CrescentoContainer crescentoContainer;
    @BindView(R.id.sign_up_btn)
    ElevatedButton signUpBtn;
    @BindView(R.id.sign_in_btn)
    ElevatedButton signInBtn;
    @BindView(R.id.input_and_controls)
    LinearLayout inputAndControls;

    public SplashFragment() {
        // Required empty public constructor
    }

    public static SplashFragment newInstance() {
        SplashFragment fragment = new SplashFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onResume() {
        super.onResume();

        fragmentNavigation.hideTabBar();
        fragmentNavigation.hideActionBar();
    }

    @Override
    public void onPause() {
        super.onPause();

        fragmentNavigation.showTabBar();
        fragmentNavigation.showActionBar();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        signInBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentNavigation.pushFragment(SignInFragment.newInstance());
            }
        });


        signUpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                fragmentNavigation.pushFragment(SignUpFragment.newInstance());
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_splash;
    }

}
