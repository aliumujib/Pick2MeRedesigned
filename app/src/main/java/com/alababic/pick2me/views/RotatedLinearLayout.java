package com.alababic.pick2me.views;

import android.content.Context;
import android.graphics.Canvas;
import android.text.TextPaint;
import android.util.AttributeSet;
import android.widget.LinearLayout;

/**
 * Created by aliumujib on 06/04/2018.
 */

public class RotatedLinearLayout extends LinearLayout {

    boolean topDown = false;

    public RotatedLinearLayout(Context context) {
        super(context);
    }

    public RotatedLinearLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public RotatedLinearLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }


    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(heightMeasureSpec, widthMeasureSpec);
        setMeasuredDimension(getMeasuredHeight(), getMeasuredWidth());
    }

    @Override
    protected void onDraw(Canvas canvas) {
        canvas.save();

        if (topDown) {
            canvas.translate(getWidth(), 0);
            canvas.rotate(180);
        } else {
            canvas.translate(0, getHeight());
            canvas.rotate(-180);
        }
        //canvas.translate(getCompoundPaddingLeft(), getExtendedPaddingTop());
        this.draw(canvas);
        canvas.restore();
    }
}


