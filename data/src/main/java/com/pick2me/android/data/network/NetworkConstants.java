package com.pick2me.android.data.network;


import java.util.HashMap;
import java.util.Map;

/**
 * Created by Abdul-Mujeeb Aliu  on 10-10-2017
 * <p>
 * All constants that will be used for web services
 */
public class NetworkConstants {
    public static final String BASE_URL = "http://tracking.alababic.com/v1.0/en/usd/";

    public static final String BASE_RELATIVE_URL = "http://tracking.alababic.com";

    public static final String USERS_ENDPOINT = "user/";
    public static final String SHOPS_END_POINT = "catalog/sections/";
    public static final String BUY_2_ME_LIST_ENDPOINT = "buyme/list/";
    public static final String CLIENT_ID_QUERY = "client_id";

    public static Map<String, String> getHeaderMap() {
        final Map<String, String> map = new HashMap<>();
        map.put("auth_token", "");
        return map;
    }

    public static final String KEY_USER = "user";
    public static final String KEY_USERNAME = "name";
    public static final String KEY_USER_EMAIL = "email";
    public static final String KEY_AUTH_TOKEN = "auth_token";

    public static final String KEY_SHOP_ID = "id";
    public static final String KEY_SHOP_CODE = "code";
    public static final String KEY_SHOP_NAME = "name";
    public static final String KEY_SHOP_SECTION_PG_URL = "section_page_url";
    public static final String KEY_SHOP_PHOTO_OBJECT = "picture";
    public static final String KEY_SHOP_PHOTO = "src";
    public static final String KEY_SHOP_PHOTO_CONTENT_TYPE = "content_type";

    public static final String KEY_P2M_ID = "id";
    public static final String KEY_P2M_PHOTO_OBJ = "photo";
    public static final String KEY_P2M_NAME = "name";
    public static final String KEY_P2M_DESCRIPTION = "description";
    public static final String KEY_P2M_WEBSITE = "website";
    public static final String KEY_P2M_PRICE = "price";
    public static final String KEY_P2M_COUNT = "count";
    public static final String KEY_P2M_ADDRESS_FROM = "address_from";
    public static final String KEY_P2M_DELIVER_TO = "deliver_to";
    public static final String KEY_P2M_FEE = "fee";
    public static final String KEY_P2M_USER_OBJECT = "user";
    public static final String KEY_P2M_USER_ID = "id";
    public static final String KEY_P2M_USER_NAME = "name";
    public static final String KEY_P2M_USER_SURNAME = "last_name";
    public static final String KEY_P2M_USER_EMAIL = "email";
    public static final String KEY_P2M_USER_AVATAR = "avatar";


    public static final String KEY_RESPONSE_TYPE = "success";

    public static final String KEY_RESPONSE_ERROR_MESSAGE = "error";

    public static final String KEY_TRACKS = "tracks";

    public static final String PAGE_LIMIT = "limit";


}
