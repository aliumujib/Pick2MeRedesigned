package com.pick2me.android.data.contracts;

import com.pick2me.android.data.models.CategoryEntity;
import com.pick2me.android.data.models.SubCategoryEntity;

import java.util.HashMap;
import java.util.List;

import io.reactivex.Observable;


/**
 * Created by aliumujib on 22/04/2018.
 */

public interface ICategoriesRepository {

    Observable<List<CategoryEntity>> getCategoryList(HashMap<String, Object> filters);

    Observable<List<SubCategoryEntity>> getSubCategoryList(HashMap<String, Object> filters);

}
