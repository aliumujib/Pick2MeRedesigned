package com.alababic.pick2me.ui.fragments.restaurantdetails;


import android.animation.Animator;
import android.animation.AnimatorListenerAdapter;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.AppBarLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.alababic.pick2me.R;
import com.alababic.pick2me.model.MenuItem;
import com.alababic.pick2me.ui.fragments.base.BaseSliderHeaderFragment;
import com.alababic.pick2me.utils.Utils;
import com.alababic.pick2me.views.servicegridview.ServiceGridView;

import java.util.ArrayList;
import java.util.List;

import at.blogc.android.views.ExpandableTextView;

public class RestaurantDetailsFragment extends BaseSliderHeaderFragment {

    private ExpandableTextView expandablesTextView;
    private ImageView expandCollapse;
    private View shopDetails;
    private ServiceGridView menuGrid;

    public RestaurantDetailsFragment() {
        // Required empty public constructor
    }

    public static RestaurantDetailsFragment newInstance() {
        RestaurantDetailsFragment fragment = new RestaurantDetailsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public void onViewCreated(@NonNull final View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        this.shopDetails = view.findViewById(R.id.shop_details);
        this.expandablesTextView = view.findViewById(R.id.expandableTextView);
        this.expandCollapse = view.findViewById(R.id.expand_collapse);
        this.menuGrid = view.findViewById(R.id.menu_grid);

        this.expandCollapse.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (expandablesTextView.isExpanded()) {
                    expandablesTextView.collapse();
                } else {
                    expandablesTextView.expand();
                }
            }
        });

        List<String> strings = new ArrayList<>();

        strings.add("https://images.pexels.com/photos/927444/pexels-photo-927444.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");
        strings.add("https://images.pexels.com/photos/937605/pexels-photo-937605.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");
        strings.add("https://images.pexels.com/photos/582430/pexels-photo-582430.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");
        strings.add("https://images.pexels.com/photos/907142/pexels-photo-907142.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=350");

        setSliderPhotos(strings, "");



        appbar.addOnOffsetChangedListener(new AppBarStateChangeListener() {
            @Override
            public void onStateChanged(AppBarLayout appBarLayout, State state) {
                Log.d("STATE", state.name());
                if (state == State.COLLAPSED) {
                    shopDetails.animate()
                            //.translationY(view.getHeight())
                            .scaleX(0.0f)
                            .scaleY(0.0f)
                            .setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    shopDetails.setVisibility(View.GONE);
                                }
                            });

                    applyMargins(Utils.dpToPx(0));

                } else {
                    shopDetails.animate()
                            //.translationY(-view.getHeight())
                            .scaleX(1.0f)
                            .scaleY(1.0f)
                            .setDuration(300)
                            .setListener(new AnimatorListenerAdapter() {
                                @Override
                                public void onAnimationEnd(Animator animation) {
                                    super.onAnimationEnd(animation);
                                    shopDetails.setVisibility(View.VISIBLE);
                                }
                            });

                    applyMargins(Utils.dpToPx(80));

                }
            }
        });

        List<MenuItem> shops = new ArrayList<>();
        shops.add(new MenuItem(0, "Menu 1"));
        shops.add(new MenuItem(1, "Menu 2"));
        shops.add(new MenuItem(2, "Menu 3"));
        shops.add(new MenuItem(3, "Menu 4"));
        shops.add(new MenuItem(4, "Menu 5"));
        shops.add(new MenuItem(5, "Menu 6"));
        shops.add(new MenuItem(6, "Menu 7"));

        menuGrid.addData(shops);

    }

    private void applyMargins(int top) {
        FrameLayout.LayoutParams lp = (FrameLayout.LayoutParams) menuGrid.getLayoutParams();
        lp.setMargins(0, top, 0, 0);
        menuGrid.setLayoutParams(lp);
    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_restaurant_details;
    }
}
