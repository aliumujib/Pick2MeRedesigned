package com.pick2me.android.data.repositories;

import android.util.Log;


import com.pick2me.android.data.contracts.IRepoContracts;
import com.pick2me.android.data.models.Shop;
import com.pick2me.android.data.network.NetworkConstants;
import com.pick2me.android.data.repositories.services.Pick2MeHTTPClient;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;

/**
 * Created by abdulmujibaliu on 10/22/17.
 */

public class ShopRepository implements IRepoContracts.IShopsRepository {
    private String TAG = getClass().getSimpleName();

    public static IRepoContracts.IShopsRepository sharedInstance = new ShopRepository();

    @Override
    public Observable<List<Shop>> getShopData() {
       /* return mNetworkHandler.getShopService().getShops().observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io());*/
        return Observable.create(new ObservableOnSubscribe<List<Shop>>() {
            @Override
            public void subscribe(final ObservableEmitter<List<Shop>> emitter) throws Exception {
                Pick2MeHTTPClient.get(NetworkConstants.SHOPS_END_POINT, null, new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONArray response) {
                        super.onSuccess(statusCode, headers, response);
                        try {
                            List<Shop> shops = new ArrayList<>();
                            for (int i = 0; i < response.length(); i++) {
                                Shop shop = new Shop(response.getJSONObject(i));
                                shops.add(shop);
                            }
                            emitter.onNext(shops);
                        } catch (JSONException e) {
                            e.printStackTrace();
                            emitter.onError(e);
                        }
                    }

                    @Override
                    public void onSuccess(int statusCode, Header[] headers, JSONObject response) {
                        super.onSuccess(statusCode, headers, response);

                        try {
                            if (response.has(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)) {
                                emitter.onError(new Throwable(response.getString(NetworkConstants.KEY_RESPONSE_ERROR_MESSAGE)));
                            } else {
                                emitter.onError(new Throwable("Unknown error"));
                            }
                        } catch (JSONException e1) {
                            e1.printStackTrace();
                            emitter.onError(e1);
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, Throwable throwable, JSONObject errorResponse) {
                        super.onFailure(statusCode, headers, throwable, errorResponse);
                        Log.d(TAG, "" + errorResponse);
                        if (errorResponse != null) {
                            emitter.onError(new Throwable(errorResponse.toString()));
                        } else {
                            emitter.onError(new Throwable("Unknown error"));
                        }
                    }

                    @Override
                    public void onFailure(int statusCode, Header[] headers, String responseString, Throwable throwable) {
                        super.onFailure(statusCode, headers, responseString, throwable);
                        emitter.onError(new Throwable(throwable));
                    }
                });
            }
        });
    }

    public List<Shop> getShops() {
        List<Shop> shops = new ArrayList<>();

        shops.add(new Shop("Mvaron Int'l Bussiness", "Kiev, Ukraine",
                "https://www.graphicsprings.com/filestorage/stencils/29c2824f9ba81b8e219075f347cf5e6b.svg", "Superstore", 2));
        shops.add(new Shop("Kelimba Ltd", "Kiev, Ukraine",
                "http://gdj.graphicdesignjunction.com/wp-content/uploads/2013/03/business+logo+design+19+16.jpg", "Food items", 2));

        shops.add(new Shop("Ukraine Int'l Shopping store", "Kiev, Ukraine",
                "https://www.freelogodesign.org/img/logo-ex-7.png", "Farm", 2));

        shops.add(new Shop("Stremwer Los", "Kiev, Ukraine",
                "https://i.pinimg.com/736x/75/b4/69/75b469395dcf237d3a2a5482100e8117--triangle-logo-arrow-logo.jpg", "Restaurant", 2));

        shops.add(new Shop("Freddy Kieve", "Kiev, Ukraine",
                "https://www.graphicsprings.com/filestorage/stencils/29c2824f9ba81b8e219075f347cf5e6b.svg", "Booking agent", 2));

        shops.add(new Shop("Mobodro", "Kiev, Ukraine",
                "http://www.thebusinesslogo.com/logo-design-portfolio/cool_candy.gif", "Dairy Farm", 2));

        shops.add(new Shop("Freetown Int'l", "Kiev, Ukraine",
                "http://gdj.graphicdesignjunction.com/wp-content/uploads/2013/02/business+logo+design+18+10.jpg", "Fashion Store", 2));

        shops.add(new Shop("Telekinises Int'l ", "Kiev, Ukraine",
                "https://image.freepik.com/free-vector/abstract-business-logo_1043-162.jpg", "Electronic Store", 2));

        shops.add(new Shop("Indiananand Junk", "Kiev, Ukraine",
                "http://www.eatlogos.com/business_logos/png/vector_business_colourful_people_logo_inspiration.png", "Pizza Parlour", 2));

        shops.add(new Shop("Flsotunf Point", "Kiev, Ukraine",
                "https://s3.amazonaws.com/htw/dt-contest-entries/60162/canada-education-online-services-business-logo-design.png", "Movie Rental", 2));


        return shops;
    }
}
