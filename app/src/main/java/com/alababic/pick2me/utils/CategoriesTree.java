package com.alababic.pick2me.utils;

import com.alababic.pick2me.R;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Node;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by aliumujib on 23/04/2018.
 */

public class CategoriesTree {

    private Node<Category> rootNode = new Node<>(new Category(-2, "Root", 0));

    //
    private Node<Category> marketsNode = new Node<>(new Category(1, "markets", R.drawable.markets));
    private Node<Category> restaurantsNode = new Node<>(new Category(0, "restaurants", R.drawable.restaurants));
    private Node<Category> shoppingNode = new Node<>(new Category(2, "shopping", R.drawable.shopping));
    private Node<Category> wishesNode = new Node<>(new Category(5, "wishes", R.drawable.wishes));
    private Node<Category> buy2meNode = new Node<>(new Category(4, "Buy2Me", R.drawable.buy_2_me));
    private Node<Category> couriersNode = new Node<>(new Category(6, "Courier", R.drawable.courier));
    private Node<Category> pharmacyNode = new Node<>(new Category(3, "pharmacy", R.drawable.pharmacy));


    public static CategoriesTree categoriesTree = new CategoriesTree();

    public static CategoriesTree getInstance() {
        return categoriesTree;
    }

    static abstract class ROOT_NODE_SUBS {
        static List<Category> get() {
            List<Category> marketCategories = new ArrayList<>();
            marketCategories.add(new Category(0, "All Markets", R.drawable.all_markets));
            marketCategories.add(new Category(1, "Gastronomy", R.drawable.groceries));
            marketCategories.add(new Category(2, "Meat and Fish", R.drawable.salmon));
            marketCategories.add(new Category(3, "Bakery", R.drawable.croissant));
            marketCategories.add(new Category(4, "Beverages", R.drawable.beverages));
            marketCategories.add(new Category(5, "Coffee and Tea", R.drawable.coffee_pot));
            marketCategories.add(new Category(6, "Household", R.drawable.gloves));
            marketCategories.add(new Category(6, "Bathroom", R.drawable.soap));
            marketCategories.add(new Category(6, "Other", R.drawable.star));
            return marketCategories;
        }
    }

    static abstract class MARKETS_NODE_SUBS {
        static List<Category> get() {
            List<Category> marketCategories = new ArrayList<>();
            marketCategories.add(new Category(0, "All Markets", R.drawable.all_markets));
            marketCategories.add(new Category(1, "Gastronomy", R.drawable.groceries));
            marketCategories.add(new Category(2, "Meat and Fish", R.drawable.salmon));
            marketCategories.add(new Category(3, "Bakery", R.drawable.croissant));
            marketCategories.add(new Category(4, "Beverages", R.drawable.beverages));
            marketCategories.add(new Category(5, "Coffee and Tea", R.drawable.coffee_pot));
            marketCategories.add(new Category(6, "Household", R.drawable.gloves));
            marketCategories.add(new Category(6, "Bathroom", R.drawable.soap));
            marketCategories.add(new Category(6, "Other", R.drawable.star));
            return marketCategories;
        }
    }


    static abstract class RESTAURANT_NODE_SUBS {
        static List<Category> get() {
            List<Category> foodCategories = new ArrayList<>();
            foodCategories.add(new Category(0, "All Restaurants", R.drawable.fast_food));
            foodCategories.add(new Category(1, "Burgers", R.drawable.hamburger));
            foodCategories.add(new Category(2, "Sushi", R.drawable.sushi));
            foodCategories.add(new Category(3, "Pizza", R.drawable.pizza));
            foodCategories.add(new Category(4, "Salad", R.drawable.salads));
            foodCategories.add(new Category(5, "Noodles", R.drawable.noodles));
            foodCategories.add(new Category(6, "Meats", R.drawable.roast_chicken));
            foodCategories.add(new Category(6, "Sweets", R.drawable.piece_of_cake));
            foodCategories.add(new Category(6, "Other", R.drawable.star));

            return foodCategories;
        }
    }

    static abstract class PHARMACY_NODE_SUBS {
        static List<Node<Category>> get() {
            Node<Category> sportFitnessNode = new Node<>(new Category(0, "Sport and Fitness", "Your favorite pills with out leaving your home", R.drawable.dumbbell));
            Node<Category> medicineEquipmentNode = new Node<>(new Category(0, "Medical equipment", "Your favorite pills with out leaving your home", R.drawable.stethoscope));
            return Arrays.asList(getMomAndBabyNode(), getMedicineNode(), getBeautyCareNode(), sportFitnessNode, medicineEquipmentNode, getPetSuppliesNode());
        }

        static private Node<Category> getMomAndBabyNode() {
            Node<Category> allMomAndBabyShopsNode = new Node<>(new Category(0, "All Shops", R.drawable.baby_pacifier));
            Node<Category> babyFoodNode = new Node<>(new Category(0, "Baby food", R.drawable.feeding_bottle));
            Node<Category> babyAndMomCare = new Node<>(new Category(0, "Baby & Mom Care", R.drawable.baby_mom_care));
            Node<Category> nappyNode = new Node<>(new Category(0, "Nappy", R.drawable.nappy));
            Node<Category> babyMedicineNode = new Node<>(new Category(0, "Medical equipment", R.drawable.nasal_spray));
            Node<Category> othersNode = new Node<>(new Category(0, "Other", R.drawable.star));
            List<Node<Category>> momAndBabyNodeSubCategories = Arrays.asList(allMomAndBabyShopsNode, babyFoodNode, babyAndMomCare, nappyNode, babyMedicineNode, othersNode);
            Node<Category> momAndBabyNode = new Node<>(new Category(0, "Mom and Baby", "Your favorite pills with out leaving your home", R.drawable.feeding_bottle));
            momAndBabyNode.addChildren(momAndBabyNodeSubCategories);
            return momAndBabyNode;
        }

        static private Node<Category> getMedicineNode() {

            Node<Category> allMedicineShopsNode = new Node<>(new Category(0, "All Pharmacies", R.drawable.thermometer));
            Node<Category> eyeNode = new Node<>(new Category(0, "Eye", R.drawable.eye_drops));
            Node<Category> eNTNode = new Node<>(new Category(0, "Ear, throat, nose", R.drawable.ear));
            Node<Category> stomachNode = new Node<>(new Category(0, "Stomach", R.drawable.stomach));
            Node<Category> heartNode = new Node<>(new Category(0, "Heart", R.drawable.heart));
            Node<Category> formHimNode = new Node<>(new Category(0, "For Him", R.drawable.tie));
            Node<Category> forHerNode = new Node<>(new Category(0, "For Her", R.drawable.bow_tie));
            Node<Category> othersNode = new Node<>(new Category(0, "Other", R.drawable.allergies));

            List<Node<Category>> medicineNodeSubCategories = Arrays.asList(allMedicineShopsNode, eyeNode, eNTNode, stomachNode, heartNode, formHimNode, forHerNode, othersNode);

            Node<Category> medicineNode = new Node<>(new Category(0, "Medicine", "Your favorite pills with out leaving your home", R.drawable.medicine));
            medicineNode.addChildren(medicineNodeSubCategories);
            return medicineNode;
        }

        static private Node<Category> getBeautyCareNode() {

            Node<Category> allBeautyShopsNode = new Node<>(new Category(0, "All Shops", R.drawable.cream));
            Node<Category> organicNode = new Node<>(new Category(0, "Organic", R.drawable.basil));
            Node<Category> spaNode = new Node<>(new Category(0, "Spa", R.drawable.spa));
            Node<Category> faceNode = new Node<>(new Category(0, "Face", R.drawable.face));
            Node<Category> bodyAndBathNode = new Node<>(new Category(0, "Body and Bath", R.drawable.sponge));
            Node<Category> handNode = new Node<>(new Category(0, "Hand", R.drawable.manicure));
            Node<Category> hairNode = new Node<>(new Category(0, "Hair", R.drawable.gin));
            Node<Category> equipmentNode = new Node<>(new Category(0, "Equipment", R.drawable.grooming));
            Node<Category> othersNode = new Node<>(new Category(0, "Other", R.drawable.star));

            List<Node<Category>> beautyCareNodeSubCategories = Arrays.asList(allBeautyShopsNode, organicNode, spaNode, faceNode, bodyAndBathNode, handNode, hairNode, equipmentNode, othersNode);

            Node<Category> beautyCareNode = new Node<>(new Category(0, "Beauty care", "Your favorite pills with out leaving your home", R.drawable.cream));
            beautyCareNode.addChildren(beautyCareNodeSubCategories);
            return beautyCareNode;
        }


        static private Node<Category> getPetSuppliesNode() {
            Node<Category> allMomAndBabyShopsNode = new Node<>(new Category(0, "All Shops", R.drawable.pet_supplies));
            Node<Category> babyFoodNode = new Node<>(new Category(0, "Dog Food", R.drawable.dog_food));
            Node<Category> babyAndMomCare = new Node<>(new Category(0, "Vitamins", R.drawable.vitamins));
            Node<Category> nappyNode = new Node<>(new Category(0, "Pet clothes", R.drawable.winter_hat));
            Node<Category> babyMedicineNode = new Node<>(new Category(0, "Equipment", R.drawable.collar));
            Node<Category> othersNode = new Node<>(new Category(0, "Other", R.drawable.star));

            List<Node<Category>> petSuppliesSubCategories = Arrays.asList(allMomAndBabyShopsNode, babyFoodNode, babyAndMomCare, nappyNode, babyMedicineNode, othersNode);

            Node<Category> petSuppliesNode = new Node<>(new Category(0, "Pet supplies", "Your favorite pills with out leaving your home", R.drawable.pet_supplies));
            petSuppliesNode.addChildren(petSuppliesSubCategories);
            return petSuppliesNode;
        }
    }


    public CategoriesTree() {
        final List<Node<Category>> rootCategories = new ArrayList<>();

        rootCategories.add(marketsNode);
        marketsNode.addChildrenList(MARKETS_NODE_SUBS.get());


        rootCategories.add(restaurantsNode);
        restaurantsNode.addChildrenList(RESTAURANT_NODE_SUBS.get());


        pharmacyNode.addChildren(PHARMACY_NODE_SUBS.get());


        rootCategories.add(shoppingNode);
        rootCategories.add(wishesNode);
        rootCategories.add(buy2meNode);
        rootCategories.add(couriersNode);
        rootCategories.add(pharmacyNode);


        rootNode.addChildren(rootCategories);
    }

    public Node<Category> getRootNode() {
        return rootNode;
    }

}
