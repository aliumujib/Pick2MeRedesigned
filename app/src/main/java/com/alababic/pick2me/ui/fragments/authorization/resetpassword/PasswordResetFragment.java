package com.alababic.pick2me.ui.fragments.authorization.resetpassword;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.alababic.pick2me.R;


public class PasswordResetFragment extends Fragment {

    public PasswordResetFragment() {
        // Required empty public constructor
    }


    public static PasswordResetFragment newInstance() {
        PasswordResetFragment fragment = new PasswordResetFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null) {

        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_password_reset, container, false);
    }

}
