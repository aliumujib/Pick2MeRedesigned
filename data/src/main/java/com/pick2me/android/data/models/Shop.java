package com.pick2me.android.data.models;

import android.os.Parcel;
import android.os.Parcelable;


import com.pick2me.android.data.network.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by abdulmujibaliu on 10/22/17.
 * <p>
 * "id": "2192",
 * "code": "ashan",
 * "depth_level": "1",
 * "name": "Ashan",
 * "section_page_url": "/catalog/ashan/",
 * "xml_id": ""
 */

public class Shop implements Parcelable {
    int id, depth_level;
    Picture picture;
    String name, location, imageURL, type, section_page_url, code, xml_id;
    int rating;

    public Shop(String name, String location, String imageURL, String type, int rating) {
        this.name = name;
        this.location = location;
        this.imageURL = imageURL;
        this.type = type;
        this.rating = rating;
    }

    public Shop(JSONObject object) throws JSONException {
        id = object.getInt(NetworkConstants.KEY_SHOP_ID);
        code = object.getString(NetworkConstants.KEY_SHOP_CODE);
        name = object.getString(NetworkConstants.KEY_SHOP_NAME);
        section_page_url = object.getString(NetworkConstants.KEY_SHOP_SECTION_PG_URL);
        picture = new Picture(object.getJSONObject(NetworkConstants.KEY_SHOP_PHOTO_OBJECT));
    }

    protected Shop(Parcel in) {
        id = in.readInt();
        depth_level = in.readInt();
        name = in.readString();
        location = in.readString();
        imageURL = in.readString();
        type = in.readString();
        section_page_url = in.readString();
        code = in.readString();
        xml_id = in.readString();
        rating = in.readInt();
    }

    public static final Creator<Shop> CREATOR = new Creator<Shop>() {
        @Override
        public Shop createFromParcel(Parcel in) {
            return new Shop(in);
        }

        @Override
        public Shop[] newArray(int size) {
            return new Shop[size];
        }
    };

    public String getName() {
        return name;
    }


    public String getLocation() {
        return location;
    }

    public String getImageURL() {
        if(picture!=null){
            return NetworkConstants.BASE_RELATIVE_URL + picture.getSrc();
        }else {
            return imageURL;
        }
    }

    public String getType() {
        return type;
    }

    public int getRating() {
        return rating;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeInt(id);
        parcel.writeInt(depth_level);
        parcel.writeString(name);
        parcel.writeString(location);
        parcel.writeString(imageURL);
        parcel.writeString(type);
        parcel.writeString(section_page_url);
        parcel.writeString(code);
        parcel.writeString(xml_id);
        parcel.writeInt(rating);
    }
}
