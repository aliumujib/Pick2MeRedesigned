package com.pick2me.android.data.repositories;


import com.pick2me.android.data.contracts.IUserRepository;
import com.pick2me.android.data.models.User;

import io.reactivex.Notification;
import io.reactivex.Observable;
import io.reactivex.ObservableEmitter;
import io.reactivex.ObservableOnSubscribe;
import io.reactivex.functions.Consumer;

/**
 * Created by ABDUL-MUJEEB ALIU on 27/11/2017.
 */

public class UserRepository implements IUserRepository {


    public static IUserRepository sharedInstance = new UserRepository();

    @Override
    public Observable<Notification<User>> getCurrentUser() {
        return Observable.create(new ObservableOnSubscribe<Notification<User>>() {
            @Override
            public void subscribe(final ObservableEmitter<Notification<User>> emitter) throws Exception {

            }
        });
    }

    @Override
    public Observable<Boolean> isUserSignedIn() {
        return Observable.create(new ObservableOnSubscribe<Boolean>() {
            @Override
            public void subscribe(final ObservableEmitter<Boolean> observableEmitter) throws Exception {
                getCurrentUser().subscribe(new Consumer<Notification<User>>() {
                    @Override
                    public void accept(Notification<User> userNotification) throws Exception {
                        if (userNotification.isOnError()) {
                            observableEmitter.onNext(false);
                        } else {
                            observableEmitter.onNext(true);
                        }
                    }
                });
            }
        });
    }

    @Override
    public Observable<Notification<Boolean>> saveCurrentUser(final User user) {
        return Observable.create(new ObservableOnSubscribe<Notification<Boolean>>() {
            @Override
            public void subscribe(final ObservableEmitter<Notification<Boolean>> emitter) throws Exception {

            }
        });
    }

    @Override
    public Observable<Notification<Boolean>> updateCurrentUserAuthToken(String authToken) {
        return null;
    }

}
