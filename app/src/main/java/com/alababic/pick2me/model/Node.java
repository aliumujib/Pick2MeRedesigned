package com.alababic.pick2me.model;

/**
 * Created by aliumujib on 23/04/2018.
 */


import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

public class Node<T extends Parcelable> implements Parcelable {
    private T data = null;
    private List<Node> children = new ArrayList<>();
    private Node parent = null;
    private String className;


    public Node(T data) {
        this.data = data;
    }


    protected Node(Parcel in) {
        this.className = in.readString();
        ClassLoader classLoader;
        try {
            classLoader = Class.forName(this.className).getClassLoader();
            data = in.readParcelable(classLoader);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
        children = in.createTypedArrayList(Node.CREATOR);

        //DONT READ THE PARENT IF YOU"RE PASSING THROUGH AN INTENT ...
        //parent = in.readParcelable(Node.class.getClassLoader());
    }

    public static final Creator<Node> CREATOR = new Creator<Node>() {
        @Override
        public Node createFromParcel(Parcel in) {
            return new Node(in);
        }

        @Override
        public Node[] newArray(int size) {
            return new Node[size];
        }
    };

    public void addChild(Node child) {
        child.setParent(this);
        this.children.add(child);
    }

    public void addChild(T data) {
        Node<T> newChild = new Node<>(data);
        newChild.setParent(this);
        children.add(newChild);
    }

    public void addChildren(List<Node<T>> children) {
        for (Node t : children) {
            t.setParent(this);
        }
        this.children.addAll(children);
    }

    public void addChildrenList(List<T> children) {
        for (T t : children) {
            addChild(t);
        }
    }


    public List<Node> getChildren() {
        return children;
    }

    public Node getChildAtIndex(int position) {
        return children.get(position);
    }


    public List<T> getChildrenList() {
        List<T> list = new ArrayList<>();
        for (Node child : children) {
            list.add((T) child.data);
        }

        return list;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    private void setParent(Node parent) {
        this.parent = parent;
    }

    public Node getParent() {
        return parent;
    }

    public boolean isLeaf() {
        return children.isEmpty();
    }

    public boolean isRoot() {
        return parent == null;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {
        parcel.writeString(className);
        parcel.writeParcelable(data, i);
        parcel.writeTypedList(children);

        //DONT SEND THE PARENT IF YOU"RE PASSING THROUGH AN INTENT ...
        //parcel.writeParcelable(parent, i);
    }
}