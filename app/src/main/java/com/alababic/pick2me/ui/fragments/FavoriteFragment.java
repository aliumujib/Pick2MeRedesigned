package com.alababic.pick2me.ui.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import com.alababic.pick2me.R;
import com.alababic.pick2me.internal.mvp.BaseFragment;
import com.alababic.pick2me.ui.activities.MainActivity;

import static com.alababic.pick2me.internal.mvp.BaseFragment.ARGS_INSTANCE;

public class FavoriteFragment extends BaseFragment {

    Button btnClickMe;

    int fragCount;

    public static FavoriteFragment newInstance(int instance) {
        Bundle args = new Bundle();
        args.putInt(ARGS_INSTANCE, instance);
        FavoriteFragment fragment = new FavoriteFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


        btnClickMe = (Button) view.findViewById(R.id.btn_click_me);

        Bundle args = getArguments();
        if (args != null) {
            fragCount = args.getInt(ARGS_INSTANCE);
        }

        btnClickMe.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (fragmentNavigation != null) {
                    fragmentNavigation.pushFragment(FavoriteFragment.newInstance(fragCount + 1));
                }
            }
        });

        ( (MainActivity)getActivity()).updateToolbarTitle((fragCount == 0) ? "Favorite" : "Sub Favorite "+fragCount);

    }

    @Override
    protected int getLayoutId() {
        return R.layout.fragment_favorite;
    }
}
