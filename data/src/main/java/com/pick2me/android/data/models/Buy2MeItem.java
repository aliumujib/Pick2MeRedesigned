package com.pick2me.android.data.models;


import com.pick2me.android.data.network.NetworkConstants;

import org.json.JSONException;
import org.json.JSONObject;

import java.time.LocalDateTime;

/**
 * Created by ABDUL-MUJEEB ALIU on 25/11/2017.
 */

public class Buy2MeItem {
    int id, count;
    String name, description, website, price, address_from, deliver_to, fee, photoImage;
    LocalDateTime dateActive, deadline;

    //To chnage when api design is more stable
    String username, userlastname, useremail, useravatar;


    public Buy2MeItem(JSONObject object) throws JSONException {
        id = object.getInt(NetworkConstants.KEY_P2M_ID);
        count = object.getInt(NetworkConstants.KEY_P2M_COUNT);
        if (object.has(NetworkConstants.KEY_P2M_PHOTO_OBJ)) {
            if (object.getJSONObject(NetworkConstants.KEY_P2M_PHOTO_OBJ).keys().hasNext()) {
                photoImage = object.getJSONObject(NetworkConstants.KEY_P2M_PHOTO_OBJ).getString(object.getJSONObject(NetworkConstants.KEY_P2M_PHOTO_OBJ).keys().next());
            } else {
                photoImage = "N/A";
            }
        }
        name = object.getString(NetworkConstants.KEY_P2M_NAME);
        description = object.getString(NetworkConstants.KEY_P2M_DESCRIPTION);
        website = object.getString(NetworkConstants.KEY_P2M_WEBSITE);
        price = object.getString(NetworkConstants.KEY_P2M_PRICE);
        address_from = object.getString(NetworkConstants.KEY_P2M_ADDRESS_FROM);
        deliver_to = object.getString(NetworkConstants.KEY_P2M_DELIVER_TO);
        fee = object.getString(NetworkConstants.KEY_P2M_FEE);

        //
        if (object.has(NetworkConstants.KEY_P2M_USER_OBJECT)) {
            JSONObject userObject = object.getJSONObject(NetworkConstants.KEY_P2M_USER_OBJECT);
            username = userObject.getString(NetworkConstants.KEY_P2M_USER_NAME);
            userlastname = userObject.getString(NetworkConstants.KEY_P2M_USER_SURNAME);
            useravatar = userObject.getString(NetworkConstants.KEY_P2M_USER_AVATAR);
        }
    }

    public String getPhotoImage() {
        return NetworkConstants.BASE_RELATIVE_URL + photoImage;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getWebsite() {
        return website;
    }

    public void setWebsite(String website) {
        this.website = website;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getAddress_from() {
        return address_from;
    }

    public void setAddress_from(String address_from) {
        this.address_from = address_from;
    }

    public String getDeliver_to() {
        return deliver_to;
    }

    public void setDeliver_to(String deliver_to) {
        this.deliver_to = deliver_to;
    }

    public String getFee() {
        return fee;
    }

    public void setFee(String fee) {
        this.fee = fee;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getUserlastname() {
        return userlastname;
    }

    public void setUserlastname(String userlastname) {
        this.userlastname = userlastname;
    }

    public String getUseremail() {
        return useremail;
    }

    public void setUseremail(String useremail) {
        this.useremail = useremail;
    }

    public String getUseravatar() {
        return NetworkConstants.BASE_RELATIVE_URL + useravatar;
    }

    public void setUseravatar(String useravatar) {
        this.useravatar = useravatar;
    }
}
