package com.alababic.pick2me.views.servicegridview;


import android.view.View;

import com.alababic.pick2me.model.Item;

/**
 * Created by aliumujib on 30/01/2018.
 */

public interface OnServiceClickListener {

    void onItemClickListener(Item item, View sharedView, int position);

    void onItemDeleteClickListener(Item item, int position);

    void onItemQtyCountChanged(Item item, int position, int quantity);

    void onItemCommentBtnClicked(Item item, int position);

}
