package com.pick2me.android.data.contracts;


import com.pick2me.android.data.models.Buy2MeItem;

import java.util.List;

import io.reactivex.Notification;
import io.reactivex.Observable;

/**
 * Created by ABDUL-MUJEEB ALIU on 25/11/2017.
 */

public interface IBuy2MeRepository {

    Observable<List<Buy2MeItem>> getBuy2MeListData();

    Observable<Notification<Boolean>> saveBuy2Me();

}
