package com.alababic.pick2me.ui.fragments.home;

import com.alababic.pick2me.internal.mvp.contract.Presentable;
import com.alababic.pick2me.internal.mvp.contract.Viewable;
import com.alababic.pick2me.model.Category;
import com.alababic.pick2me.model.Node;

import java.util.List;

/**
 * Created by aliumujib on 23/04/2018.
 */

public interface HomeContract {

    interface View extends Viewable<Presenter> {
        void setCategoryData(List<Category> categoryData);

        void initMenuViews(List<Category> categoryData);

        void gotoNextFragment(Node<Category> category, int index);

        void showMenu();
    }

    interface Presenter extends Presentable<View> {

        void getCategoryData();

        void sethasintialized(boolean hasInit);

        void presentDetailFragment(int index);
    }

}
