package com.alababic.pick2me.internal.di.categorydetailfragment;


import com.alababic.pick2me.internal.di.app.components.ApplicationComponent;
import com.alababic.pick2me.ui.fragments.categorydetails.CategoriesDetailFragment;

import dagger.Component;

@Component(dependencies = ApplicationComponent.class, modules = CategoryModule.class)
public interface CategoryDetailComponent {

    void inject(CategoriesDetailFragment fragment);

}
