package com.alababic.pick2me.internal.di.homefragment;


import com.alababic.pick2me.internal.di.app.components.ApplicationComponent;
import com.alababic.pick2me.ui.fragments.home.HomeFragment;

import dagger.Component;

@Component(dependencies = ApplicationComponent.class, modules = HomeModule.class)
public interface HomeComponent {

//    void inject(SignUpFragment fragment);
//
//    void inject(SignInFragment fragment);
//
//    void inject(PasswordResetFragment fragment);
//
//    void inject(SplashFragment fragment);

    void inject(HomeFragment fragment);

    //void inject(CategoriesDetailFragment fragment);


}
