package com.pick2me.android.data.repositories.services;

/**
 * Created by abdulmujibaliu on 11/17/17.
 */

import android.content.Context;
import android.util.Log;

import com.pick2me.android.data.network.NetworkConstants;
import com.loopj.android.http.*;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.HttpEntity;
import cz.msebera.android.httpclient.message.BasicHeader;

import static com.loopj.android.http.AsyncHttpClient.LOG_TAG;

public class Pick2MeHTTPClient {

    private static AsyncHttpClient client = new AsyncHttpClient();

    public Pick2MeHTTPClient() {
        client.setLoggingEnabled(true);
        client.setLoggingLevel(LogInterface.VERBOSE);
    }

    public List<Header> getRequestHeadersList() {
        List<Header> headers = new ArrayList<Header>();

        String headersRaw = "Content-Type=application/json";

        if (headersRaw != null && headersRaw.length() > 3) {
            String[] lines = headersRaw.split("\\r?\\n");
            for (String line : lines) {
                try {
                    int equalSignPos = line.indexOf('=');
                    if (1 > equalSignPos) {
                        throw new IllegalArgumentException("Wrong header format, may be 'Key=Value' only");
                    }

                    String headerName = line.substring(0, equalSignPos).trim();
                    String headerValue = line.substring(1 + equalSignPos).trim();
                    Log.d(LOG_TAG, String.format("Added header: [%s:%s]", headerName, headerValue));

                    headers.add(new BasicHeader(headerName, headerValue));
                } catch (Throwable t) {
                    Log.e(LOG_TAG, "Not a valid header line: " + line, t);
                }
            }
        }
        return headers;
    }

    public Header[] getRequestHeaders() {
        List<Header> headers = getRequestHeadersList();
        return headers.toArray(new Header[headers.size()]);
    }

    public static void get(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.get(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(String url, RequestParams params, AsyncHttpResponseHandler responseHandler) {
        client.post(getAbsoluteUrl(url), params, responseHandler);
    }

    public static void post(Context context, String url, HttpEntity params, String contentType, AsyncHttpResponseHandler responseHandler) {
        client.post(context, getAbsoluteUrl(url), params, contentType, responseHandler);
    }

    private static String getAbsoluteUrl(String relativeUrl) {
        return NetworkConstants.BASE_URL + relativeUrl;
    }
}