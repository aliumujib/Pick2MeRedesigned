package com.pick2me.android.data.contracts;

import com.pick2me.android.data.models.CartItem;
import com.pick2me.android.data.models.ProductEntity;

import java.util.List;

import io.reactivex.Observable;


/**
 * Created by abdulmujibaliu on 11/13/17.
 */

public interface ICartRepository {

    void addItemToCart(ProductEntity productItem);


    Observable<Integer> getCartCount();

    void removeItemFromCart(ProductEntity productItem);


    Observable<List<CartItem>> getCartItems();
}
